#include "dal.h"

#include <stdio.h>
#include <string.h>

extern volatile int globalApplicationFinished;

char STOP_EV[] = "stop_state1";

extern int DAL_send_event(void *message, DALProcess *p) {
	char event_str[256];
	strcpy(event_str, (const char *)message);
	// HACK: hard-coded, as this behavior does not exist in PRUNE
	// TODO: figure out how DAL maps the user-defined event into the exit
	// command and attempt to mimic
	// TODO: all this is probably not required not that the actor::fire return
	// value defines exit status
	if (strcmp(event_str, STOP_EV) == 0) {
		printf("executing `globalApplicationFinished = 1;`...\n");
		globalApplicationFinished = 1;
	} else {
		printf("DAL_send_event is not yet implemented for command %s!\n", event_str);
	}
	// TODO: what should this return?
	return 0;
}
