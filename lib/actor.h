#pragma once

#include "common.h"
#include "fifo.h"

#define T_GPU 0
#define T_CPU 1

typedef struct {
	int device;
	char const *name;
	int iteration;
	int core;

	fifo_t **inputs;
	int inputCount;

	fifo_t **outputs;
	int outputCount;

	// one of gpu_actor_params_t, cpu_actor_params_t
	void *data;
} actor_t;

extern volatile int globalApplicationFinished;

void actorReset(actor_t *actor);
void actorFree(actor_t *actor);
void actorSetName(actor_t *actor, char const *name);
void actorAllocate(actor_t *actor, int device, void *data);
void actorAddInput(actor_t *actor, fifo_t *fifo);
void actorAddOutput(actor_t *actor, fifo_t *fifo);
void actorFire(actor_t *actor);
void *actorWrapper(void *p);
void actorSetCore(actor_t *actor, int core);
void actorInitializeShared(actor_t *actor, shared_t *shared);
void actorFreeShared(shared_t *shared);
void actorSetGPUParams(gpu_actor_params_t *params, cl_command_queue commands, cl_kernel kernel,
					   int dimensions, size_t *globalSize, size_t *localSize);
void actorSetIoFunction(actor_t *actor, gpu_actor_params_t *params,
						PROCESS_RETURN_T (*ioFunction)(PROCESS_PTR_T));
void actorSetConstantData(cl_context context, gpu_actor_params_t *params, int argInd,
						  const char *name);
