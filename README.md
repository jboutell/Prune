PRUNE dataflow environment for heterogeneous computing
------------------------------------------------------

PRUNE is a dataflow-based programming environment for heterogeneous computing. The core of PRUNE is a run-time library that provides a dataflow API for creating efficient applications for Linux-based multicore+GPU computers.

Applications are written in a combination of XML and C: individual actors are described in C, and the actor interconnection network in XML. Additionally, the mapping of actors to CPU cores or to the GPU is specified in another XML file that specifies the mapping.

Overall, PRUNE consists of the following components:
- The PRUNE runtime library that provides the dataflow API for actors
- The PRUNE compiler that compiles the XML files into C
- The PRUNE analyzer that analyzes XML files for correctness


Publications
------------
Boutellier J, Wu J, Huttunen H and Bhattacharyya SS (2018) PRUNE: dynamic and decidable dataflow for signal processing on heterogeneous platforms in IEEE Transactions on Signal Processing, 66(3)

Boutellier J and Bhattacharyya SS (2017) Low-power heterogeneous computing via adaptive execution of dataflow actors IEEE International Workshop on Signal Processing Systems (SiPS)

Boutellier J and Hautala I (2016) Executing dynamic data rate actor networks on OpenCL platforms IEEE International Workshop on Signal Processing Systems (SiPS)


New user's guide
----------------
Compiling the PRUNE compiler and the related applications requires standard C & C++ compilers and CMake 2.8 (tested with gcc and clang).

### Project organization
* `compiler/` -directory contains the PRUNE-compiler that compiles XML into C-specifications.
* `applications/` -directory contains applications made using the PRUNE environment.
* `lib/` -directory contains the runtime library.

### Compiling the PRUNE compiler
The PRUNE compiler is required for compiling any of the applications:

```
compiler/scripts/build.sh
```

### Compiling an application
1. Generate the top-level executable and the related CMakeLists.txt, e.g.

    ```
    compiler/scripts/run-dnn-cpu.sh
    ```

2. Generate platform dependent makefiles. Note that you can change the search directories for the input files using the CMake cache variables. You can also customize the OpenCL device indices using the `SELECTED_DEVICE` and the `SELECTED_PLATFORM` cache variables (find indices using the `clinfo` tool).

    ```
    cd applications/dnn-cpu
    mkdir -p build && cd build
    cmake ..
    ```

3. Build the application:

    ```
    make
    ```

4. Run the binary. Note that the execution working directory must be the directory with the generated sources directory.

    ```
    cd ..
    bin/dnn
    ```

### Converting a DAL application to PRUNE
Note that the process is not difficult, but it may not be trivial either. Many of these compatibility issues are to be fixed in future releases.

1. Create the application directory (inside the PRUNE repository), e.g.

    ```
    mkdir -p applications/my-dal-app && cd applications/my-dal-app
    ```

2. Copy over the xml-files, e.g.

    ```
    mkdir -p xml && cp -r <dal-app-dir>/app1/*.xml xml/
    ```

3. Copy over the DAL-implementation, e.g.

    ```
    cp -r <dal-app-dir>/app1/src src
    ```

4. If there are empty headers in the src directory, they are very likely to be headers created to shadow some library functions for some workaround left from the earlier days. Remove these shadowing headers (eg. common.h and ports.h in dyndpd) from the now copied over DAL-implementation, e.g. `rm src/common.h src/ports.h`.
5. There are a number of unresolved compatibility issues between PRUNE and DAL. They are listed here with suggested and successfully implemented fixes:
    * A problem arises when the generated `src-gen/dyndpd.cpp` has to `#include` all of the actor
      definitions, while the DAL version does not. Because of this you need to do the following: for
      each of the headers in the copied over DAL-implementation, remove the `_local_states` part of
      the actor typedef. You can easily achieve this by opening vim with `vim -p src/*.h` and running

        ```
        :bufdo %s/_local_states//ge | update
        :qa
        ```
    * DAL uses some pre-compilation magic to desugar strings defined in a specific manner into the
      port identifiers of the process network, which PRUNE does not do. A workaround for now is to
      run the following command with `vim -p src/*.h` to remove the port definitions from the header:

        ```
        :bufdo g/^#define PORT_/d | update
        :qa
        ```
    The next step is to rename the port definitions in the C source files. This can be done using
    `vim -p src/*.c` with:

        ```
        :bufdo %s/PORT_/\= substitute(expand('%:t:r'), ".*", '\U&_', 'g')/ge | update
        :qa
        ```
    This captures most if not all of the port definitions. If some remain, they'll become apparent
    during compilation.
    * The DALProcess::local variable may have to be explicitly cast to the correct destination type
      in PRUNE actor sources. Note the style of the reference:
      `some_State *data = (some_State*)(&p->local);`. Missing one of these reference signs may
      cause a segfault.
    * You may have to include the generated "ports.h" file in the PRUNE version of actor sources.
    * Other compatibility issues may exist, but they are mostly shallow in nature. Debugging them should be
      easy by tracking what symbols exist and what do not.
6.  PRUNE does not support variables in network or platform XMLs.

#### Running the converted DAL-application
Starting with the PRUNE repository root as the working directory...

1. Create directories for generated files, eg.

    ```
    mkdir applications/my-dal-app/src-gen
    ```

2. Start the PRUNE-compiler from the `compiler` directory with the `--dal-api` flag, eg.

    ```
    cd compiler
    ## parameters are: <input-platform.xml> <input-network.xml> <input-mapping.xml> <output-Makefile> <output-toplevel.c> <output-ports.h> <output-vertices.h>
    bin/prune-compile ../platforms/octacore_with_gpu.xml ../applications/my-dal-app/xml/pn.xml ../applications/my-dal-app/xml/mapping1.xml ../applications/my-dal-app/CMakeLists.txt ../applications/my-dal-app/src-gen/<app-name>.cpp ../applications/my-dal-app/src-gen/ports.h ../applications/my-dal-app/src-gen/vertices.h --dal-api
    ```

3. Make an out-of-source build-directory in the application directory

    ```
    cd ../applications/my-dal-app
    mkdir -p build && cd build
    ```

3. Run cmake. Note that the search directories specified in CMake cache variables do not work for applications converted in this way, and input files should be placed at your home directory. You can customize the OpenCL device indices, however (find indices using the `clinfo` tool).

    ```
    cmake ..
    ```

4. Build using `make`

#### Troubleshooting
Here's a collection of potential compatibility issues and possible solutions:
* Some DAL-applications may re-define OpenCL symbols like `cl_int`. OpenCL is compiled into the application binary in an earlier stage in PRUNE than in DAL. This results in a namespace conflict in PRUNE. The problem can be circumvented by using the following code that works in both frameworks:

    ```
    #ifndef DAL_API
    // problematic definitions
    #endif
    ```

* The DAL versions of the applications seem to not currently implement functions that are shared between files, a function might thus be implemented multiple times in the DAL-version. Often an implementation of such a function already exists in PRUNE and just removing the function definitions from the PRUNE application might fix the problem. If not however, one might have to move the implementation of such a function into it's own header (or inside PRUNE-lib) and include that header.


License
-------
PRUNE is provided under the Apache 2.0 license

