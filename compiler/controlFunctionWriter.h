#pragma once

#include "fileWriter.h"
#include "network.h"

typedef struct {
	std::vector<Actor *> *a;
	std::vector<port *> *DRPx;
	std::vector<port *> *DRPy;
	bool isDC;
} dC;
typedef struct {
	Actor *q;
	Actor *x;
	Actor *y;
	std::vector<dC *> *DCs;
} dGraph;

class ControlFunctionWriter : public FileWriter {
  private:
	Network *network;

  public:
	ControlFunctionWriter(Network *network);
	~ControlFunctionWriter();
	void writeFile(dGraph *DPG, Actor *a, bool isX);
};
