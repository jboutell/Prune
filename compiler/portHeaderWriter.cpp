#include "portHeaderWriter.h"
#include <cstdio>
#include <cstring>

#define MAX_STR 512

PortHeaderWriter::PortHeaderWriter(Network *network) {
	this->network = network;
}

PortHeaderWriter::~PortHeaderWriter() {}

void PortHeaderWriter::writeFile() {
	fprintf(file, "#pragma once\n\n");
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			for (unsigned int j = 0; j < v->portCount(); j++) {
				port *p = network->getVertexByIndex(i)->getPort(j);
				int di = p->index;
				char tmp[MAX_STR];
				upperCase(tmp, p->name);
				if (p->direction & INPUT) {
					fprintf(file, "#define %s %i\n", tmp, di);
					fprintf(file, "#define %s_input_%i_name %s\n", v->getName(), di, p->name);
					fprintf(file, "#define %s_input_%i_type %s_type\n", v->getName(), di, p->name);
					fprintf(file, "\n");
				} else if (p->direction == OUTPUT) {
					fprintf(file, "#define %s %i\n", tmp, di);
					fprintf(file, "#define %s_output_%i_name %s\n", v->getName(), di, p->name);
					fprintf(file, "#define %s_output_%i_type %s_type\n", v->getName(), di, p->name);
					fprintf(file, "\n");
				}
			}
		}
	}
}
