#pragma once

#define MAX_ITERATIONS 10000

class Array {
  private:
	Array(){};

  public:
	static unsigned int isInArray(unsigned int *array, unsigned int integerCount,
								  unsigned int integer);
	static unsigned int tryAddArray(unsigned int *array, unsigned int *integerCount,
									unsigned int integer);
	static void sortArray(unsigned int *array, unsigned int integer_count);
};
