#pragma once

#include "genericXMLReader.h"
#include "platform.h"

class ipxactPlatformXMLreader : GenericXMLReader {
  private:
	Platform *storePlatform(xmlTextReaderPtr reader);
	void readPlatformDetails(Platform *pform, xmlTextReaderPtr reader);
	void storeName(Platform *pform, xmlTextReaderPtr reader);
	void storeComponentInstances(Platform *pform, xmlTextReaderPtr reader);
	void readComponentInstancesDetails(Platform *pform, xmlTextReaderPtr reader);
	void readComponentInstanceDetails(Platform *pform, xmlTextReaderPtr reader);
	void storeInstanceName(Platform *pform, xmlTextReaderPtr reader);
	void storeVendorExtensionsProc(Platform *pform, Processor *proc, xmlTextReaderPtr reader);
	void storeVendorExtensionsShar(Platform *pform, Shared *shared, xmlTextReaderPtr reader);
	void readVendorExtensionsDetailsProc(Platform *pform, Processor *proc, xmlTextReaderPtr reader);
	void readVendorExtensionsDetailsShar(Platform *pform, Shared *shared, xmlTextReaderPtr reader);
	void storePortPositionsProc(Platform *pform, Processor *proc, xmlTextReaderPtr reader);
	void storePortPositionsShar(Platform *pform, Shared *shared, xmlTextReaderPtr reader);
	void readPortPositionsDetailsProc(Platform *pform, Processor *proc, xmlTextReaderPtr reader);
	void readPortPositionsDetailsShar(Platform *pform, Shared *shared, xmlTextReaderPtr reader);
	void readPortPositionDetailsProc(Platform *pform, Processor *proc, xmlTextReaderPtr reader);
	void readPortPositionDetailsShar(Platform *pform, Shared *shared, xmlTextReaderPtr reader);
	void storeInterconnections(Platform *pform, xmlTextReaderPtr reader);
	void readInterconnectionsDetails(Platform *pform, xmlTextReaderPtr reader);
	void readInterconnectionDetails(Platform *pform, xmlTextReaderPtr reader);
	void storeD3Name(Platform *pform, xmlTextReaderPtr reader);
	void storeActiveInterface(Platform *pform, Link *link, xmlTextReaderPtr reader);

  public:
	ipxactPlatformXMLreader(){};
	~ipxactPlatformXMLreader(){};
	Platform *readPlatform(const char *fileName);
};
