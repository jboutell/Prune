#pragma once

// the libxml2 API header i.e. #include <libxml/xmlreader.h> is included in
// genericXMLReader.h
#include "genericXMLReader.h"
#include "platform.h"

class XMLFormatDetector {

  public:
	XMLFormatDetector(){};
	~XMLFormatDetector(){};
	bool detectFormat(const char *fileName);
};
