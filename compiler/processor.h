#pragma once

#include "vertex.h"

#define PROCESSOR_CPU 0
#define PROCESSOR_GPU 1

typedef struct {
	char *name;
	char *id;
} capability;

class Processor : public Vertex {
  private:
	int processorType;
	int coreIndex;
	std::vector<capability *> *capabilities;

  public:
	Processor();
	~Processor();
	void setProcessorType(int type);
	void extractCoreId();
	int getCoreId();
	int getProcessorType();
	void addCapability(char *name, char *id);
};
