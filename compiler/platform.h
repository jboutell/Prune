#pragma once

#include "graph.h"
#include "link.h"
#include "processor.h"
#include "shared.h"

class Platform : public Graph {
  public:
	Platform(){};
	~Platform(){};
	Processor *addProcessor(char *name);
	Shared *addShared(char *name);
	Link *addLink(char *name);
};
