#include "controlFunctionWriter.h"
#include <cstdio>

#define MAX_STR 512

ControlFunctionWriter::ControlFunctionWriter(Network *network) {
	this->network = network;
}

ControlFunctionWriter::~ControlFunctionWriter() {}

void ControlFunctionWriter::writeFile(dGraph *DPG, Actor *a, bool isX) {
	char tmp[MAX_STR];
	std::vector<bool> portPrinted;
	for (unsigned int i = 0; i < a->portCount(); i++) {
		portPrinted.push_back(false);
	}
	fprintf(file, "#include \"common.h\"\n");
	fprintf(file, "#include \"ports.h\"\n");
	fprintf(file, "\n");
	fprintf(file, "void *%sIO(void *p) {\n", DPG->x->getName());
	fprintf(file, "\tio_data_t *data = (io_data_t *) p;\n");
	fprintf(file, "\n");
	fprintf(file, "\tint n_ind = data->cValue;\n");
	fprintf(file, "\n");
	if (isX) {
		for (unsigned int i = 0; i < DPG->DCs->size(); i++) {
			for (unsigned int j = 0; j < DPG->DCs->at(i)->DRPx->size(); j++) {
				port *p = DPG->DCs->at(i)->DRPx->at(j);
				upperCase(tmp, p->name);
				fprintf(file, "\tdata->outputEnabled[%s] = (n_ind & %i) > 0;\n", tmp, 1 << (i - 1));
				portPrinted[a->getPortNum(p->hash)] = true;
			}
		}
	} else {
		for (unsigned int i = 0; i < DPG->DCs->size(); i++) {
			for (unsigned int j = 0; j < DPG->DCs->at(i)->DRPy->size(); j++) {
				port *p = DPG->DCs->at(i)->DRPy->at(j);
				upperCase(tmp, p->name);
				fprintf(file, "\tdata->inputEnabled[%s] = (n_ind & %i) > 0;\n", tmp, 1 << (i - 1));
				portPrinted[a->getPortNum(p->hash)] = true;
			}
		}
	}
	for (unsigned int i = 0; i < portPrinted.size(); i++) {
		if (portPrinted[i] == false) {
			port *p = a->getPort(i);
			upperCase(tmp, p->name);
			if (p->direction & INPUT) {
				fprintf(file, "\tdata->inputEnabled[%s] = 1;\n", tmp);
			} else if (p->direction == OUTPUT) {
				fprintf(file, "\tdata->outputEnabled[%s] = 1;\n", tmp);
			}
		}
	}
	fprintf(file, "\n");
	fprintf(file, "\treturn p;\n");
	fprintf(file, "}\n");
}
