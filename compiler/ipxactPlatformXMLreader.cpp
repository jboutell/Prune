#include "ipxactPlatformXMLreader.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

void ipxactPlatformXMLreader::storeActiveInterface(Platform *pform, Link *link,
												   xmlTextReaderPtr reader) {
#ifdef DEBUG
	const unsigned char *name = xmlTextReaderConstName(reader);
#endif
	unsigned char *component = xmlTextReaderGetAttribute(reader, (unsigned char *)"componentRef");
	unsigned char *bus = xmlTextReaderGetAttribute(reader, (unsigned char *)"busRef");
	if ((component != NULL) && (bus != NULL)) {
#ifdef DEBUG
		printf("\t\tD3 %s found \n", name);
		printf("\t\t- C %s, R %s found\n", component, bus);
#endif
		ipxactStorePort(link, link->getName(), reader);
	}
	free(component);
	free(bus);
}

void ipxactPlatformXMLreader::storeD3Name(Platform *pform, xmlTextReaderPtr reader) {
#ifdef DEBUG
	const unsigned char *name = xmlTextReaderConstName(reader);
#endif
	unsigned char *linkName = xmlTextReaderReadString(reader);
	if (linkName != NULL) {
#ifdef DEBUG
		printf("\t\tD3 %s found \n", name);
		printf("\t\t- It is %s \n", linkName);
#endif
		Link *link = pform->addLink((char *)linkName);

		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 2) {
				break;
			}
			storeActiveInterface(pform, link, reader);
			ret = xmlTextReaderRead(reader);
		}
	}
	free(linkName);
}

void ipxactPlatformXMLreader::readInterconnectionDetails(Platform *pform, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 3) {
		if (!strcmp((char *)name, "ipxact:name")) {
			storeD3Name(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:activeInterface")) {
			// storeActiveInterface(pform, reader);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void ipxactPlatformXMLreader::readInterconnectionsDetails(Platform *pform,
														  xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 2) {
		if (!strcmp((char *)name, "ipxact:interconnection")) {
#ifdef DEBUG
			printf("\tD2 %s found\n", (char *)name);
#endif
			int ret = xmlTextReaderRead(reader);
			while (ret == 1) {
				if (xmlTextReaderDepth(reader) == 2) {
					break;
				}
				readInterconnectionDetails(pform, reader);
				ret = xmlTextReaderRead(reader);
			}
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void ipxactPlatformXMLreader::storeInterconnections(Platform *pform, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (name != NULL) {
#ifdef DEBUG
		printf("D1 %s found\n", name);
#endif
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 1) {
				break;
			}
			readInterconnectionsDetails(pform, reader);
			ret = xmlTextReaderRead(reader);
		}
	} else {
		printf("No interconnections entry found\n");
	}
}

void ipxactPlatformXMLreader::readPortPositionDetailsShar(Platform *pform, Shared *shared,
														  xmlTextReaderPtr reader) {
	unsigned char *PortPositionBusRef =
		xmlTextReaderGetAttribute(reader, (unsigned char *)"busRef");
	unsigned char *PortPositionX = xmlTextReaderGetAttribute(reader, (unsigned char *)"x");
	unsigned char *PortPositionY = xmlTextReaderGetAttribute(reader, (unsigned char *)"y");
	if ((PortPositionBusRef != NULL) && (PortPositionX != NULL) && (PortPositionY != NULL)) {
#ifdef DEBUG
		printf("\t\t\t\t- B %s, X %s, Y %s \n", PortPositionBusRef, PortPositionX, PortPositionY);
#endif
		ipxactStorePort(shared, shared->getName(), reader);
	}
	free(PortPositionBusRef);
	free(PortPositionX);
	free(PortPositionY);
}

void ipxactPlatformXMLreader::readPortPositionDetailsProc(Platform *pform, Processor *proc,
														  xmlTextReaderPtr reader) {
	unsigned char *PortPositionBusRef =
		xmlTextReaderGetAttribute(reader, (unsigned char *)"busRef");
	unsigned char *PortPositionX = xmlTextReaderGetAttribute(reader, (unsigned char *)"x");
	unsigned char *PortPositionY = xmlTextReaderGetAttribute(reader, (unsigned char *)"y");
	if ((PortPositionBusRef != NULL) && (PortPositionX != NULL) && (PortPositionY != NULL)) {
#ifdef DEBUG
		printf("\t\t\t\t- B %s, X %s, Y %s \n", PortPositionBusRef, PortPositionX, PortPositionY);
#endif
		ipxactStorePort(proc, proc->getName(), reader);
	}
	free(PortPositionBusRef);
	free(PortPositionX);
	free(PortPositionY);
}

void ipxactPlatformXMLreader::readPortPositionsDetailsShar(Platform *pform, Shared *shared,
														   xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 5) {
		if (!strcmp((char *)name, "kactus2:portPosition")) {
#ifdef DEBUG
			printf("\t\t\t\tD5 %s found\n", (char *)name);
#endif
			readPortPositionDetailsShar(pform, shared, reader);
		}
	}
}

void ipxactPlatformXMLreader::readPortPositionsDetailsProc(Platform *pform, Processor *proc,
														   xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 5) {
		if (!strcmp((char *)name, "kactus2:portPosition")) {
#ifdef DEBUG
			printf("\t\t\t\tD5 %s found\n", (char *)name);
#endif
			readPortPositionDetailsProc(pform, proc, reader);
		}
	}
}

void ipxactPlatformXMLreader::storePortPositionsShar(Platform *pform, Shared *shared,
													 xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (name != NULL) {
#ifdef DEBUG
		printf("\t\t\tD4 %s found\n", (char *)name);
#endif
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 4) {
				break;
			}
			readPortPositionsDetailsShar(pform, shared, reader);
			ret = xmlTextReaderRead(reader);
		}
	} else {
		printf("No PortPositions entry found\n");
	}
}

void ipxactPlatformXMLreader::storePortPositionsProc(Platform *pform, Processor *proc,
													 xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (name != NULL) {
#ifdef DEBUG
		printf("\t\t\tD4 %s found\n", (char *)name);
#endif
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 4) {
				break;
			}
			readPortPositionsDetailsProc(pform, proc, reader);
			ret = xmlTextReaderRead(reader);
		}
	} else {
		printf("No PortPositions entry found\n");
	}
}

void ipxactPlatformXMLreader::readVendorExtensionsDetailsShar(Platform *pform, Shared *shared,
															  xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 4) {
		if (!strcmp((char *)name, "kactus2:uuid")) {
			// storeUuid(pform, reader);
		} else if (!strcmp((char *)name, "kactus2:portPositions")) {
			storePortPositionsShar(pform, shared, reader);
		} else if (!strcmp((char *)name, "kactus2:position")) {
			// storePosition(pform, reader);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {

		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void ipxactPlatformXMLreader::readVendorExtensionsDetailsProc(Platform *pform, Processor *proc,
															  xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 4) {
		if (!strcmp((char *)name, "kactus2:uuid")) {
			// storeUuid(pform, reader);
		} else if (!strcmp((char *)name, "kactus2:portPositions")) {
			storePortPositionsProc(pform, proc, reader);
		} else if (!strcmp((char *)name, "kactus2:position")) {
			// storePosition(pform, reader);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {

		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void ipxactPlatformXMLreader::storeVendorExtensionsShar(Platform *pform, Shared *shared,
														xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (name != NULL) {
#ifdef DEBUG
		printf("\t\tD3 %s found\n", name);
#endif
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 3) {
				break;
			}
			readVendorExtensionsDetailsShar(pform, shared, reader);
			ret = xmlTextReaderRead(reader);
		}
	} else {
		printf("No VendorExtensions entry found\n");
	}
}

void ipxactPlatformXMLreader::storeVendorExtensionsProc(Platform *pform, Processor *proc,
														xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (name != NULL) {
#ifdef DEBUG
		printf("\t\tD3 %s found\n", name);
#endif
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 3) {
				break;
			}
			readVendorExtensionsDetailsProc(pform, proc, reader);
			ret = xmlTextReaderRead(reader);
		}
	} else {
		printf("No VendorExtensions entry found\n");
	}
}

void ipxactPlatformXMLreader::storeInstanceName(Platform *pform, xmlTextReaderPtr reader) {
#ifdef DEBUG
	const unsigned char *name = xmlTextReaderConstName(reader);
#endif
	unsigned char *componentName = xmlTextReaderReadString(reader);
	Processor *proc = NULL;
	Shared *shared = NULL;
	if (componentName != NULL) {
#ifdef DEBUG
		printf("\t\tD3 %s found \n", name);
		printf("\t\t- It is named %s \n", componentName);
#endif
		if (!strncmp((char *)componentName, "core_", 5)) {
			proc = pform->addProcessor((char *)componentName);
#ifdef DEBUG
			printf("\t\t- The %s is added to the processor object\n", componentName);
#endif
			int ret = xmlTextReaderRead(reader);
			while (ret == 1) {
				const unsigned char *vxname = xmlTextReaderConstName(reader);
				if (xmlTextReaderDepth(reader) == 2) {
					break;
				}

				if (!strcmp((char *)vxname, "ipxact:vendorExtensions")) {
					storeVendorExtensionsProc(pform, proc, reader);
				}
				ret = xmlTextReaderRead(reader);
			}
		} else if (!strncmp((char *)componentName, "wishbone_bus", 12)) {
			shared = pform->addShared((char *)componentName);
#ifdef DEBUG
			printf("\t\t- The %s is added to the shared object\n", componentName);
#endif
			int ret = xmlTextReaderRead(reader);
			while (ret == 1) {
				const unsigned char *vxname = xmlTextReaderConstName(reader);
				if (xmlTextReaderDepth(reader) == 2) {
					break;
				}

				if (!strcmp((char *)vxname, "ipxact:vendorExtensions")) {
					storeVendorExtensionsShar(pform, shared, reader);
				}
				ret = xmlTextReaderRead(reader);
			}
		} else {
			printf("Unknown instance name encountered\n");
		}
	}
	free(componentName);
}

void ipxactPlatformXMLreader::readComponentInstanceDetails(Platform *pform,
														   xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 3) {
		if (!strcmp((char *)name, "ipxact:instanceName")) {
			storeInstanceName(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:componentRef")) {
			// storeComponentRef(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:vendorExtensions")) {
			// storeVendorExtensions(pform, reader);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void ipxactPlatformXMLreader::readComponentInstancesDetails(Platform *pform,
															xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 2) {
		if (!strcmp((char *)name, "ipxact:componentInstance")) {

#ifdef DEBUG
			printf("\tD2 %s found\n", (char *)name);
#endif
			int ret = xmlTextReaderRead(reader);
			while (ret == 1) {
				if (xmlTextReaderDepth(reader) == 2) {
					break;
				}
				readComponentInstanceDetails(pform, reader);
				ret = xmlTextReaderRead(reader);
			}
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {

		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void ipxactPlatformXMLreader::storeComponentInstances(Platform *pform, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (name != NULL) {
#ifdef DEBUG
		printf("D1 %s found\n", name);
#endif
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 1) {
				break;
			}
			readComponentInstancesDetails(pform, reader);
			ret = xmlTextReaderRead(reader);
		}
	} else {
		printf("No ComponentInstances entry found\n");
	}
}

void ipxactPlatformXMLreader::storeName(Platform *pform, xmlTextReaderPtr reader) {
#ifdef DEBUG
	const unsigned char *name = xmlTextReaderConstName(reader);
#endif
	unsigned char *pformName = xmlTextReaderReadString(reader);
	if (pformName != NULL) {
#ifdef DEBUG
		printf("D1 %s found\n", name);
		printf("- It is %s\n", pformName);
#endif
		pform->setName((char *)pformName);
	}
	free(pformName);
}

void ipxactPlatformXMLreader::readPlatformDetails(Platform *pform, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 1) {
		if (!strcmp((char *)name, "ipxact:vendor")) {
			// storeVendor(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:library")) {
			// storeLibrary(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:name")) {
			storeName(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:version")) {
			// storeVersion(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:componentInstances")) {
			storeComponentInstances(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:interconnections")) {
			storeInterconnections(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:description")) {
			// storeDescription(pform, reader);
		} else if (!strcmp((char *)name, "ipxact:vendorExtensions")) {
			// storeD1VendorExtensions(pform, reader);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {

		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

Platform *ipxactPlatformXMLreader::storePlatform(xmlTextReaderPtr reader) {
	Platform *pform = new Platform();
	int ret = xmlTextReaderRead(reader);
	while (ret == 1) {
		if (xmlTextReaderDepth(reader) == 0) {
			break;
		}
		readPlatformDetails(pform, reader);
		ret = xmlTextReaderRead(reader);
	}
	return pform;
}

Platform *ipxactPlatformXMLreader::readPlatform(const char *fileName) {
	Platform *pform = NULL;
	LIBXML_TEST_VERSION
	xmlTextReaderPtr reader = xmlReaderForFile(fileName, NULL, 0);
	if (reader != NULL) {
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			const unsigned char *name = xmlTextReaderConstName(reader);
			if (!strcmp((char *)name, "ipxact:design")) {
				pform = storePlatform(reader);
				pform->connectUndirectedGraph();
			} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
			} else {
				printf("unknown name %s encountered\n", (char *)name);
			}
			ret = xmlTextReaderRead(reader);
		}
		xmlFreeTextReader(reader);
	} else {
		printf("Unable to open %s\n", fileName);
	}
	xmlCleanupParser();
	xmlMemoryDump();
	return pform;
}
