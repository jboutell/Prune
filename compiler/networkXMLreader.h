#pragma once

#include "genericXMLReader.h"
#include "link.h"
#include "network.h"

class NetworkXMLReader : public GenericXMLReader {
  private:
	void storeSource(Actor *proc, xmlTextReaderPtr reader);
	void storeConstantData(Actor *proc, xmlTextReaderPtr reader);
	void readProcessDetails(Actor *proc, xmlTextReaderPtr reader);
	void readChannelDetails(Fifo *shared, xmlTextReaderPtr reader);
	void readConnectionDetails(Link *link, xmlTextReaderPtr reader);
	void storeProcess(Network *network, xmlTextReaderPtr reader);
	void storeChannel(Network *network, xmlTextReaderPtr reader);
	void storeConnection(Network *network, xmlTextReaderPtr reader);
	void readNetworkDetails(Network *network, xmlTextReaderPtr reader);
	Network *storeNetwork(xmlTextReaderPtr reader);

  public:
	NetworkXMLReader(){};
	~NetworkXMLReader(){};
	Network *readNetwork(const char *fileName);
};
