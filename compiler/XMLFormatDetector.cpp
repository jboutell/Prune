#include "XMLFormatDetector.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
//#define DEBUG

// XMLFormatDetector function is called to see if the format of the input XML
// file is DAL or IP-XACT using libxml2 API (Application Programming Interface)
// see http://www.xmlsoft.org/  the libxml2 API header i.e. #include
// <libxml/xmlreader.h> is included in genericXMLReader.h XMLFormatDetector
// function receives the address of the xml file and detects its format
bool XMLFormatDetector::detectFormat(const char *fileName) {

	int format = 0;

	// macro to check that the libxml version in use is compatible with the
	// version the software has been compiled against
	LIBXML_TEST_VERSION

	// xmlReaderForFile function belongs to libxml2 API
	// xmlReaderForFile parse the XML file and returns the new reader or NULL in
	// case of error xmlTextReaderPtr is a pointer type defined to refere to
	// xmlTextReader structure i.e. Typedef xmlTextReader * xmlTextReaderPtr
	// xmlTextReader structure is not made public by API
	xmlTextReaderPtr reader = xmlReaderForFile(fileName, NULL, 0);

	// if the xml file is parsed successfully
	if (reader != NULL) {

		// xmlTextReaderRead moves the position of the current instance to the next
		// node in the stream, exposing its properties  xmlTextReaderRead function
		// returns 1 if the next node is read successfully  xmlTextReaderRead
		// function returns 0 if there is no more node to read xmlTextReaderRead
		// function returns -1 in case of error
		int ret = xmlTextReaderRead(reader);

		// if the next node is read successfully
		if (ret == 1) {

			// xmlTextReaderConstName function reads a node and returns its name or
			// NULL if not available
			const unsigned char *name = xmlTextReaderConstName(reader);

			// if the name of the read node is "architecture"
			// strcmp returns 0 when the contents of both strings are equal
			//(char *) is type casting of name to char pointer
			if (!strcmp((char *)name, "architecture")) {
				printf("The DAL XML format is detected and the topmost node is %s \n",
					   (char *)name);
				format = false;

			} else if (!strcmp((char *)name, "ipxact:design")) {
				printf("The IP-XACT XML format is detected and the topmost node is %s \n",
					   (char *)name);
				format = true;

			} else {
				printf("unknown format %s encountered\n", (char *)name);
			}

			// xmlFreeTextReader function deallocate all of the resources associated
			// to the reader
			xmlFreeTextReader(reader);

			// if the read xml file doesn't contain any node then it prints an error
			// message
		} else {
			printf("Unable to read the topmost node in %s\n", fileName);
		}

	} else {
		printf("Unable to open the file %s\n", fileName);
	}

	xmlCleanupParser();
	xmlMemoryDump();

	return format;
}
