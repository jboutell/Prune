#pragma once

#include "binding.h"
#include "network.h"
#include "platform.h"

class Mapping : public Entity {
  private:
	std::vector<Edge *> *edges;

  public:
	Mapping();
	~Mapping();
	Binding *addBinding(char *name);
	void connectBindings(Platform *platform, Network *network);
};
