#pragma once

#include "network.h"
#include <cstdio>

class FileWriter {
  protected:
	FILE *file;
	Network *network;

	void addIfNotPresent(std::vector<char *> *strVect, char *str);
	void printFilenames(const char *format);

  public:
	bool openFile(const char *fileName);
	void closeFile();
	char *upperCase(char *sbuffer, char *str);
};
