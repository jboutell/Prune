#include "platformXMLreader.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
//#define DEBUG

void PlatformXMLReader::storeCapability(Processor *proc, xmlTextReaderPtr reader) {
	unsigned char *capabilityName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	unsigned char *capabilityId = xmlTextReaderGetAttribute(reader, (unsigned char *)"identifier");
	if ((capabilityName != NULL) && (capabilityId != NULL)) {
#ifdef DEBUG
		printf("Capability %s of id %s found\n", capabilityName, capabilityId);
#endif
		proc->addCapability((char *)capabilityName, (char *)capabilityId);
	} else {
		printf("Capability entry with incomplete attributes found\n");
	}
	free(capabilityName);
	free(capabilityId);
}

void PlatformXMLReader::readProcessorDetails(Processor *proc, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 2) {
		if (!strcmp((char *)name, "port")) {
			storePort(proc, proc->getName(), reader);
		} else if (!strcmp((char *)name, "capability")) {
			storeCapability(proc, reader);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void PlatformXMLReader::readSharedDetails(Shared *shared, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 2) {
		if (!strcmp((char *)name, "port")) {
			storePort(shared, shared->getName(), reader);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void PlatformXMLReader::readLinkDetails(Link *link, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 2) {
		if (!strcmp((char *)name, "end_point_1")) {
			storeEndPoint(link, reader, 1);
		} else if (!strcmp((char *)name, "end_point_2")) {
			storeEndPoint(link, reader, 2);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

// storeProcessor function
void PlatformXMLReader::storeProcessor(Platform *pform, xmlTextReaderPtr reader) {

	// xmlTextReaderGetAttribute function receives the pointer to the parsed
	// structure and the name of the attribute  xmlTextReaderGetAttribute function
	// returns an string containing the value of the specified attribute, or NULL
	// in case of error, The string must be deallocated by the caller
	unsigned char *processorName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");

	// xmlTextReaderGetAttribute function receives the pointer to the parsed
	// structure and the id of the attribute
	unsigned char *processorId = xmlTextReaderGetAttribute(reader, (unsigned char *)"id");

	// xmlTextReaderGetAttribute function receives the pointer to the parsed
	// structure and the type of the attribute
	unsigned char *processorType = xmlTextReaderGetAttribute(reader, (unsigned char *)"type");

	if ((processorName != NULL) && (processorId != NULL) && (processorType != NULL)) {

#ifdef DEBUG
		printf("Processor %s of id %s and type %s found\n", processorName, processorId,
			   processorType);
#endif

		// addProcessor function is a member function of Platform class
		// addProcessor function calls setName, setResourceType, addBindingPort and
		// push_back functions
		Processor *proc = pform->addProcessor((char *)processorName);

		// atoi function convert string to integer
		// setId function is a member function of Entity class
		proc->setId((atoi((char *)processorId)));

		// strcmp function compares the strings and returns 0 if both strings are
		// equal
		if (strcmp((char *)processorType, "GPU") == 0) {

			// setProcessorType function is a member function of Processor class
			// PROCESSOR_GPU is defined in processor.h as #define PROCESSOR_GPU 1
			proc->setProcessorType(PROCESSOR_GPU);
		} else {
			// PROCESSOR_CPU is defined in processor.h as #define PROCESSOR_CPU 0
			proc->setProcessorType(PROCESSOR_CPU);

			// extractCoreId function is a member function of Processor class
			proc->extractCoreId();
		}

		// reads the next node
		int ret = xmlTextReaderRead(reader);

		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 1) {
				break;
			}

			readProcessorDetails(proc, reader);

			// reads the next node
			ret = xmlTextReaderRead(reader);
		}

	} else {
		printf("Processor entry with incomplete attributes found\n");
	}
	free(processorName);
	free(processorId);
	free(processorType);
}

void PlatformXMLReader::storeShared(Platform *pform, xmlTextReaderPtr reader) {
	unsigned char *sharedName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	if (sharedName != NULL) {
#ifdef DEBUG
		printf("Shared resource %s found\n", sharedName);
#endif
		Shared *shared = pform->addShared((char *)sharedName);
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 1) {
				break;
			}
			readSharedDetails(shared, reader);
			ret = xmlTextReaderRead(reader);
		}
	}
	free(sharedName);
}

void PlatformXMLReader::storeLink(Platform *pform, xmlTextReaderPtr reader) {
	unsigned char *linkName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	if (linkName != NULL) {
#ifdef DEBUG
		printf("Link %s found\n", linkName);
#endif
		Link *link = pform->addLink((char *)linkName);
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 1) {
				break;
			}
			readLinkDetails(link, reader);
			ret = xmlTextReaderRead(reader);
		}
	}
	free(linkName);
}

// readPlatformDetails function reads the nodes with depth 1
void PlatformXMLReader::readPlatformDetails(Platform *pform, xmlTextReaderPtr reader) {

	// xmlTextReaderConstName function reads a node and returns its name or NULL
	// if not available
	const unsigned char *name = xmlTextReaderConstName(reader);

	// xmlTextReaderDepth function returns the depth of the node in the tree or -1
	// in case of error if the depth of the node is 1 i.e. the nodes connected to
	// the root node
	if (xmlTextReaderDepth(reader) == 1) {

		// if the name of the read node is "processor"
		// strcmp returns 0 when the contents of both strings are equal
		//(char *) is type casting of name to char pointer
		if (!strcmp((char *)name, "processor")) {

			// storeProcessor is a member function of PlatformXMLReader class
			storeProcessor(pform, reader);

			// if the name of the read node is "shared"
		} else if (!strcmp((char *)name, "shared")) {

			// storeShared is a member function of PlatformXMLReader class
			storeShared(pform, reader);

			// if the name of the read node is "link"
		} else if (!strcmp((char *)name, "link")) {

			// storeLink is a member function of PlatformXMLReader class
			storeLink(pform, reader);

			// xmlTextReaderNodeType function returns the node type as an integer
			// number see
			// http://www.gnu.org/software/dotgnu/pnetlib-doc/System/Xml/XmlNodeType.html
			// for the list of return values  xmlTextReaderNodeType returns -1 in case
			// of error xmlTextReaderNodeType returns 14 in case of significant white
			// space the text skip is defined as 14 i.e. #define TEXTSKIP 14 in
			// genericXMLReader.h file i.e. White space between markup in a mixed
			// content model or white space within the xml:space="preserve" scope
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {

		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

// storePlatform function receives a pointer to the parsed structure and returns
// a pointer to the object of the filled Platform class
Platform *PlatformXMLReader::storePlatform(xmlTextReaderPtr reader) {

	// member functions declaration of Platform class is in platform.h
	Platform *pform = NULL;

	// xmlTextReaderGetAttribute function receives the pointer to the parsed
	// structure and the name of the attribute  xmlTextReaderGetAttribute function
	// returns an string containing the value of the specified attribute, or NULL
	// in case of error, The string must be deallocated by the caller
	unsigned char *pformName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");

	// if the value of the attribute is read successfully
	if (pformName != NULL) {

#ifdef DEBUG
		printf("Platform %s found\n", pformName);
#endif

		// the dynamic memory is allocated by new operand and its pointer is stored
		// in pform
		pform = new Platform();

		// "->" operator accesses the member fields of an object e.g. member
		// functions of a class object  setName function sets or resets the name of
		// the node  setName function sets the name attribute of the node to the
		// platform
		pform->setName((char *)pformName);

		// xmlTextReaderRead moves the position of the current instance to the next
		// node in the stream, exposing its properties  xmlTextReaderRead function
		// returns 1 if the next node is read successfully  xmlTextReaderRead
		// function returns 0 if there is no more node to read xmlTextReaderRead
		// function returns -1 in case of error
		int ret = xmlTextReaderRead(reader);

		// if the next node is read successfully
		while (ret == 1) {

			// xmlTextReaderDepth function returns the depth of the node in the tree
			// or -1 in case of error  if the depth of the node is 0 i.e. the root
			// node
			if (xmlTextReaderDepth(reader) == 0) {
				break;
			}

			// readPlatformDetails function is a member function of PlatformXMLReader
			// class
			//
			readPlatformDetails(pform, reader);

			// reads the next node
			ret = xmlTextReaderRead(reader);
		}
	}

	// deallocating pformName
	free(pformName);

	// returns the pointer to the object of the Platform class which contains the
	// xml file name attributes and details
	return pform;
}

// readPlatform function is called to read the input xml file describing the
// platform using libxml2 API (Application Programming Interface)  see
// http://www.xmlsoft.org/  the libxml2 API header i.e. #include
// <libxml/xmlreader.h> is included in genericXMLReader.h  readPlatform function
// receives the address of the xml file and reads its nodes
Platform *PlatformXMLReader::readPlatform(const char *fileName) {

	// member functions declaration of Platform class is in platform.h
	Platform *pform = NULL;

	// macro to check that the libxml version in use is compatible with the
	// version the software has been compiled against
	LIBXML_TEST_VERSION

	// xmlReaderForFile function belongs to libxml2 API
	// xmlReaderForFile parse the XML file and returns the new reader or NULL in
	// case of error xmlTextReaderPtr is a pointer type defined to refere to
	// xmlTextReader structure i.e. Typedef xmlTextReader * xmlTextReaderPtr
	// xmlTextReader structure is not made public by API
	xmlTextReaderPtr reader = xmlReaderForFile(fileName, NULL, 0);

	// if the xml file is parsed successfully
	if (reader != NULL) {

		// xmlTextReaderRead moves the position of the current instance to the next
		// node in the stream, exposing its properties  xmlTextReaderRead function
		// returns 1 if the next node is read successfully  xmlTextReaderRead
		// function returns 0 if there is no more node to read xmlTextReaderRead
		// function returns -1 in case of error
		int ret = xmlTextReaderRead(reader);

		// if the next node is read successfully
		while (ret == 1) {

			// xmlTextReaderConstName function reads a node and returns its name or
			// NULL if not available
			const unsigned char *name = xmlTextReaderConstName(reader);

			// if the name of the read node is "architecture"
			// strcmp returns 0 when the contents of both strings are equal
			//(char *) is type casting of name to char pointer
			if (!strcmp((char *)name, "architecture")) {
				// storePlatform function is a member function of PlatformXMLReader
				// class storePlatform function receives the pointer to parsed structure
				// and returns a pointer to ...
				pform = storePlatform(reader);

				// connectUndirectedGraph function is a member function of Graph class
				// connectUndirectedGraph function dosen't receive any arguments and
				// returns void
				pform->connectUndirectedGraph();

				// xmlTextReaderNodeType function returns the node type as an integer
				// number see
				// http://www.gnu.org/software/dotgnu/pnetlib-doc/System/Xml/XmlNodeType.html
				// for the list of return values  xmlTextReaderNodeType returns -1 in
				// case of error xmlTextReaderNodeType returns 14 in case of significant
				// white space the text skip is defined as 14 i.e. #define TEXTSKIP 14
				// in genericXMLReader.h file  i.e. White space between markup in a
				// mixed content model or white space within the xml:space="preserve"
				// scope
			} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
				// do noting !!!

			} else {
				printf("unknown name %s encountered\n", (char *)name);
			}
			// reads the next node
			ret = xmlTextReaderRead(reader);
		}

		// xmlFreeTextReader function deallocate all of the resources associated to
		// the reader
		xmlFreeTextReader(reader);

		// if the read xml file doesn't contain any node then it prints an error
		// message
	} else {
		printf("Unable to open %s\n", fileName);
	}

	xmlCleanupParser();
	xmlMemoryDump();

	return pform;
}
