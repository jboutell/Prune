#pragma once

#include "genericXMLReader.h"
#include "platform.h"

class PlatformXMLReader : GenericXMLReader {
  private:
	void storeCapability(Processor *proc, xmlTextReaderPtr reader);
	void readProcessorDetails(Processor *proc, xmlTextReaderPtr reader);
	void readSharedDetails(Shared *shared, xmlTextReaderPtr reader);
	void readLinkDetails(Link *link, xmlTextReaderPtr reader);
	void storeProcessor(Platform *pform, xmlTextReaderPtr reader);
	void storeShared(Platform *pform, xmlTextReaderPtr reader);
	void storeLink(Platform *pform, xmlTextReaderPtr reader);
	void readPlatformDetails(Platform *pform, xmlTextReaderPtr reader);
	Platform *storePlatform(xmlTextReaderPtr reader);

  public:
	PlatformXMLReader(){};
	~PlatformXMLReader(){};
	Platform *readPlatform(const char *fileName);
};
