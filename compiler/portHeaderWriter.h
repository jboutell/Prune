#pragma once

#include "fileWriter.h"
#include "network.h"

class PortHeaderWriter : public FileWriter {
  private:
	Network *network;

  public:
	PortHeaderWriter(Network *network);
	~PortHeaderWriter();
	void writeFile();
};
