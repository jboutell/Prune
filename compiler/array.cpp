#include "array.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

unsigned int Array::isInArray(unsigned int *array, unsigned int integerCount,
							  unsigned int integer) {
	for (unsigned int i = 0; i < integerCount; i++) {
		if (array[i] == integer) {
			return 1;
		}
	}
	return 0;
}

unsigned int Array::tryAddArray(unsigned int *array, unsigned int *integerCount,
								unsigned int integer) {
	if (!isInArray(array, integerCount[0], integer)) {
		array[integerCount[0]++] = integer;
		return 1;
	}
	return 0;
}

void Array::sortArray(unsigned int *array, unsigned int integer_count) {
	unsigned int deadlock_counter = MAX_ITERATIONS;
	unsigned int found_unsorted = 1;

	while (found_unsorted && deadlock_counter) {
		unsigned int i;
		found_unsorted = 0;
		for (i = 0; i < integer_count - 1; i++) {
			if (array[i] > array[i + 1]) {
				found_unsorted = 1;
				break;
			}
		}

		if (found_unsorted) {
			unsigned int tmp;
			tmp = array[i];
			array[i] = array[i + 1];
			array[i + 1] = tmp;
		}

		deadlock_counter--;
	}
	if (deadlock_counter == 0) {
		printf("*** Error: deadlock in Array::sortArray\n");
	}
}
