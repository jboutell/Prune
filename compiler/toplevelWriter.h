#pragma once

#include "fileWriter.h"
#include "platform.h"
#include <cstdio>

class ToplevelWriter : public FileWriter {
  private:
	Platform *platform;
	bool useDalApi;
	bool useKrnFn;
	void getKernelName(Actor *a, char *krnName);
	void printResourceStrings(const char *str, int resourceType, int deviceType);
	void printConnectionStrings();
	void printConnectionString(Vertex *v, bool isConfigurationPort);
	void printAllocateFifos(int fifoType);
	void printDeviceParamDecls(const char *str, int deviceType);
	void printActorParamDecls(const char *str);
	void printActorToDeviceParamAssignments(const char *str, int deviceType, int actorType);
	void printActorToGPUParamAssignments(const char *str, int deviceType, int actorType);
	void printConstantAssignments(int deviceType);
	void printSourceToDeviceParamAssignments(const char *str, int deviceType);
	void printSourceAssignments(const char *str, int deviceType);
	void printDeviceAssignments(const char *str, int deviceType);
	void printDeviceIndices(const char *str, int deviceType);
	void printSetIOFunction(const char *str);
	void printActorCalls(const char *str, int deviceType, int actorType);
	void printActorSetCore(const char *str);
	void printWithEach(const char *fmt, ...);

  public:
	ToplevelWriter(Platform *platform, Network *network, bool useDalApi, bool useKrnFn);
	~ToplevelWriter() {}
	void writeFile();
};
