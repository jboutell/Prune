#include "vertex.h"

Vertex::Vertex() : resourceType(PROCESSOR), index(0) {
	predecessors = new std::vector<unsigned int>;
	successors = new std::vector<unsigned int>;
}

Vertex::~Vertex() {
	delete predecessors;
	delete successors;
}

Vertex::Vertex(const Vertex &obj) : Entity(obj) {
	predecessors = new std::vector<unsigned int>(*obj.predecessors);
	successors = new std::vector<unsigned int>(*obj.successors);
	resourceType = obj.resourceType;
	index = obj.index;
}

int Vertex::getResourceType() {
	return resourceType;
}
void Vertex::setResourceType(int type) {
	resourceType = type;
}

void Vertex::addSuccessor(unsigned int vertexIndex) {
	successors->push_back(vertexIndex);
}

void Vertex::addPredecessor(unsigned int vertexIndex) {
	predecessors->push_back(vertexIndex);
}

void Vertex::addConnected(unsigned int vertexIndex) {
	addPredecessor(vertexIndex);
	addSuccessor(vertexIndex);
}

unsigned int Vertex::getPredecessorCount() {
	return predecessors->size();
}

unsigned int Vertex::getPredecessor(unsigned int predecessorIndex) {
	return predecessors->at(predecessorIndex);
}

void Vertex::setPredecessor(unsigned int predecessorIndex, unsigned int newPredecessor) {
	predecessors->at(predecessorIndex) = newPredecessor;
}

unsigned int Vertex::getSuccessorCount() {
	return successors->size();
}

unsigned int Vertex::getSuccessor(unsigned int successorIndex) {
	return successors->at(successorIndex);
}

void Vertex::setSuccessor(unsigned int successorIndex, unsigned int newSuccessor) {
	successors->at(successorIndex) = newSuccessor;
}

void Vertex::removeSuccessor(unsigned int vertexIndex) {
	for (unsigned int i = 0; i < successors->size(); i++) {
		if (successors->at(i) == vertexIndex) {
			successors->erase(successors->begin() + i);
			return;
		}
	}
}

void Vertex::removePredecessor(unsigned int vertexIndex) {
	for (unsigned int i = 0; i < predecessors->size(); i++) {
		if (predecessors->at(i) == vertexIndex) {
			predecessors->erase(predecessors->begin() + i);
			return;
		}
	}
}

void Vertex::setIndex(int index) {
	this->index = index;
}

int Vertex::getIndex() {
	return index;
}
