#include "genericXMLReader.h"
#include <cstring>

port *GenericXMLReader::ipxactStorePort(Entity *entity, char *pointName, xmlTextReaderPtr reader) {
	unsigned char *busR = xmlTextReaderGetAttribute(reader, (unsigned char *)"busRef");
	port *newPort = NULL;
	if (busR != NULL) {
		newPort = entity->addPort(pointName, (char *)busR);
#ifdef DEBUG
		printf(" Port %s stored\n", busR);
#endif
	} else {
#ifdef DEBUG
		printf(" Unkown port found\n");
#endif
	}
	free(busR);
	return newPort;
}

port *GenericXMLReader::storePort(Entity *entity, char *pointName, xmlTextReaderPtr reader) {
	unsigned char *portName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	unsigned char *portType = xmlTextReaderGetAttribute(reader, (unsigned char *)"type");
	port *newPort = NULL;

	if (portName != NULL) {
		newPort = entity->addPort(pointName, (char *)portName);
		if (portType != NULL) {
			if (!strcmp((char *)portType, "input")) {
				newPort->direction = INPUT;
#ifdef DEBUG
				printf(" Input port %s found\n", portName);
#endif
			} else if (!strcmp((char *)portType, "output")) {
				newPort->direction = OUTPUT;
#ifdef DEBUG
				printf(" Output port %s found\n", portName);
#endif
			} else {
				printf(" Output port %s with unknown direction %s found\n", portName, portType);
			}
		} else {
#ifdef DEBUG
			printf(" Port %s found\n", portName);
#endif
		}
	}
	free(portName);
	free(portType);
	return newPort;
}

void GenericXMLReader::readEndPointDetails(Link *link, xmlTextReaderPtr reader,
										   unsigned char *pointName, int num) {
	const unsigned char *name = xmlTextReaderConstName(reader);

	if (xmlTextReaderDepth(reader) == 3) {
		if (!strcmp((char *)name, "port")) {
			port *newPort = storePort(link, (char *)pointName, reader);
			newPort->direction = num;
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("GenericXMLReader::readEndPointDetails: unknown name %s "
				   "encountered\n",
				   (char *)name);
		}
	}
}

void GenericXMLReader::storeEndPoint(Link *link, xmlTextReaderPtr reader, int num) {
	unsigned char *pointName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	if (pointName != NULL) {
#ifdef DEBUG
		printf("Endpoint %s found\n", pointName);
#endif
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 2) {
				break;
			}
			readEndPointDetails(link, reader, pointName, num);
			ret = xmlTextReaderRead(reader);
		}
	}
	free(pointName);
}
