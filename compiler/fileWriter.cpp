#include "fileWriter.h"
#include <algorithm>
#include <cctype>
#include <cstring>

bool FileWriter::openFile(const char *fileName) {
	file = fopen(fileName, "w");
	if (!file) {
		return false;
	}
	return true;
}

void FileWriter::closeFile() {
	fclose(file);
	file = NULL;
}

void FileWriter::addIfNotPresent(std::vector<char *> *strVect, char *str) {
	bool found = false;
	for (unsigned int i = 0; i < strVect->size(); i++) {
		if (!strcmp(strVect->at(i), str)) {
			found = true;
		}
	}
	if (!found) {
		strVect->push_back(str);
	}
}

void FileWriter::printFilenames(const char *format) {
	if (!network) {
		printf("Error printing filenames: network not set\n");
		return;
	}
	std::vector<char *> *sources = new std::vector<char *>;
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			bool printFile = true;
			Actor *a = static_cast<Actor *>(v);
			if (a->getProcessor() != NULL) {
				if (a->getProcessor()->getProcessorType() == PROCESSOR_GPU &&
					(a->getDynamic() == NULL)) {
					printFile = false;
				}
			}
			if (printFile) {
				addIfNotPresent(sources, a->getSource());
			}
		}
	}
	for (unsigned int i = 0; i < sources->size(); i++) {
		fprintf(file, format, sources->at(i));
	}
	delete sources;
}

char *FileWriter::upperCase(char *sbuffer, char *str) {
	int len = strlen(str);
	for (int i = 0; i < len; i++) {
		sbuffer[i] = toupper(str[i]);
	}
	sbuffer[len] = '\0';
	return sbuffer;
}
