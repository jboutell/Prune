#include "writeCMakeLists.h"

#include "network.h"
#include "writeTemplate.h"
#include <unordered_map>

using namespace std;

namespace TemplateProcessing {

/*
 * This function lists out all the replacements to be done for the template.
 */
Error create_replacements(Network *application, unordered_map<string, string> *replacements,
						  bool useDalApi) {
	replacements->insert({"{{APPNAME}}", application->getName()});
	if (useDalApi)
		replacements->insert({"{{PREPROCESSOR_DEFINITION_LINES}}", "add_definitions(-DDAL_API)"});
	else
		replacements->insert({"{{PREPROCESSOR_DEFINITION_LINES}}", ""});

	return make_error(NO_ERROR, "");
}

Error writeCMakeLists(string const &templateFilename, Network *application, bool useDalApi,
					  string *output) {

	if (application == NULL)
		return make_error(INVALID_PARAMETER, "application must not be NULL");
	if (output == NULL)
		return make_error(INVALID_PARAMETER, "output must not be NULL");

	// Create map of replacements
	unordered_map<string, string> replacements;
	auto error = create_replacements(application, &replacements, useDalApi);
	if (error.code != 0)
		return error;

	// Render template
	error = write(templateFilename, replacements, output);
	if (error.code != 0)
		return error;

	return make_error(NO_ERROR, "");
}

} // namespace TemplateProcessing
