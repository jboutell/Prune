#include "render.h"

using namespace std;

namespace TemplateProcessing {

typedef unordered_map<std::string, std::string> ReplacementMap;
typedef ReplacementMap::const_iterator ReplacementMapConstIter;

const char LINESKIP_SYM = '!';

void apply_replacements(string *line, ReplacementMap const &replacements) {
	ReplacementMapConstIter it = replacements.begin();
	ReplacementMapConstIter end = replacements.end();
	for (; it != end; ++it) {
		const string &tag = it->first;
		const string &replacement = it->second;

		// Find and replace instances of tag in the line
		for (size_t tag_at = line->find(tag); tag_at != string::npos; tag_at = line->find(tag)) {
			line->replace(tag_at, tag.length(), replacement);
			tag_at = line->find(tag);
		}
	}
}

void render(ifstream &templateFile, unordered_map<string, string> const &ReplacementMap,
			string *output) {
	string line;
	while (getline(templateFile, line)) {
		// Ignore lines starting with the line-skip symbol
		if (line.length() != 0 && line[0] == LINESKIP_SYM)
			continue;

		apply_replacements(&line, ReplacementMap);
		(*output).append(line + '\n');
	}
}

} // namespace TemplateProcessing
