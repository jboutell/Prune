/*
 * Internal file for aggregating generic functionality for template writers.
 */
#pragma once

#include "error.h"
#include <string>
#include <unordered_map>

namespace TemplateProcessing {

Error write(std::string const &templateFilename,
			std::unordered_map<std::string, std::string> const &replacements, std::string *output);
}
