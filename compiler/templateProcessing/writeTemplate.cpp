/*
 * Internal file for aggregating generic functionality for template writers.
 */
#include "writeTemplate.h"
#include "error.h"
#include "templateProcessing/render.h"
#include <fstream>

using namespace std;
using namespace TemplateProcessing;

namespace TemplateProcessing {

Error write(std::string const &templateFilename,
			std::unordered_map<std::string, std::string> const &replacements, std::string *output) {

	ifstream templateFile(templateFilename);

	// Detect file not found
	if (!templateFile.good())
		return make_error(FILE_NOT_FOUND, "template file not found " + templateFilename);

	// Catch other errors
	if (!templateFile.is_open())
		return make_error(UNKNOWN_ERROR, "unknown error");

	// Render the file with the substitutions to the variable
	render(templateFile, replacements, output);
	templateFile.close();

	return make_error(NO_ERROR, "");
}

} // namespace TemplateProcessing
