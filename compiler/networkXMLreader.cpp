#include "networkXMLreader.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

void NetworkXMLReader::storeSource(Actor *proc, xmlTextReaderPtr reader) {
	unsigned char *sourceType = xmlTextReaderGetAttribute(reader, (unsigned char *)"type");
	unsigned char *sourceLocation = xmlTextReaderGetAttribute(reader, (unsigned char *)"location");
	if ((sourceType != NULL) && (sourceLocation != NULL)) {
#ifdef DEBUG
		printf(" Source %s of id %s found\n", sourceType, sourceLocation);
#endif
		if (!strcmp((char *)sourceType, "cl")) {
			proc->setDeviceType(CL_DEVICE);
		} else if (!strcmp((char *)sourceType, "c")) {
			proc->setDeviceType(GEN_DEVICE);
		} else {
			printf("Source %s of unknown device type %s\n", proc->getName(), sourceType);
		}
		proc->setSource((char *)sourceLocation);
	} else {
		printf("Source entry with incomplete attributes found\n");
	}
	free(sourceType);
	free(sourceLocation);
}

void NetworkXMLReader::storeConstantData(Actor *proc, xmlTextReaderPtr reader) {
	unsigned char *dataLocation = xmlTextReaderGetAttribute(reader, (unsigned char *)"location");
	unsigned char *dataIndex = xmlTextReaderGetAttribute(reader, (unsigned char *)"index");
	if (dataLocation != NULL) {
		int index = -1;
		if (dataIndex != NULL) {
			sscanf((const char*)dataIndex, "%i", &index);
		}
#ifdef DEBUG
		printf(" Constant data in %s declared\n", dataLocation);
#endif
		proc->setConstantData((char *)dataLocation, index);
	} else {
		printf("Source entry with incomplete attributes found\n");
	}
	free(dataLocation);
}

void NetworkXMLReader::readProcessDetails(Actor *proc, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 2) {
		if (!strcmp((char *)name, "port")) {
			storePort(proc, proc->getName(), reader);
		} else if (!strcmp((char *)name, "source")) {
			storeSource(proc, reader);
		} else if (!strcmp((char *)name, "constant_data")) {
			storeConstantData(proc, reader);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void NetworkXMLReader::readChannelDetails(Fifo *shared, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 2) {
		if (!strcmp((char *)name, "port")) {
			storePort(shared, shared->getName(), reader);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void NetworkXMLReader::readConnectionDetails(Link *link, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 2) {
		if (!strcmp((char *)name, "origin")) {
			storeEndPoint(link, reader, 1);
		} else if (!strcmp((char *)name, "target")) {
			storeEndPoint(link, reader, 2);
		} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
		} else {
			printf("unknown name %s encountered\n", (char *)name);
		}
	}
}

void NetworkXMLReader::storeProcess(Network *network, xmlTextReaderPtr reader) {
	unsigned char *processName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	unsigned char *processType = xmlTextReaderGetAttribute(reader, (unsigned char *)"type");
	unsigned char *processDynamic = xmlTextReaderGetAttribute(reader, (unsigned char *)"dynamic");
	if ((processName != NULL) && (processType != NULL)) {
#ifdef DEBUG
		printf("Process %s of type %s found\n", processName, processType);
#endif
		Actor *proc = network->addActor((char *)processName);
		proc->setResourceType(ACTOR);
		if (strcmp((char *)processType, "io") == 0) {
			proc->setActorType(ACTOR_IO);
		} else if (strcmp((char *)processType, "local") == 0) {
			proc->setActorType(ACTOR_LOCAL);
		} else {
			printf("unknown type %s for actor %s encountered\n", processType, processName);
		}
		if (processDynamic != NULL) {
			proc->setDynamic((char *)processDynamic);
		}
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 1) {
				break;
			}
			readProcessDetails(proc, reader);
			ret = xmlTextReaderRead(reader);
		}
	} else {
		printf("Process entry with incomplete attributes found\n");
	}
	free(processName);
	free(processType);
}

void NetworkXMLReader::storeChannel(Network *network, xmlTextReaderPtr reader) {
	unsigned char *sharedName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	unsigned char *sharedSize = xmlTextReaderGetAttribute(reader, (unsigned char *)"size");
	unsigned char *sharedTokenSize =
		xmlTextReaderGetAttribute(reader, (unsigned char *)"tokensize");
	unsigned char *sharedDelays =
		xmlTextReaderGetAttribute(reader, (unsigned char *)"initialtokens");
	if ((sharedName != NULL) && (sharedSize != NULL)) {
#ifdef DEBUG
		printf("Channel %s found\n", sharedName);
#endif
		Fifo *shared = network->addFifo((char *)sharedName);
		shared->setResourceType(FIFO);
		if (sharedTokenSize != NULL) {
			shared->setTokenRate(atoi((char *)sharedSize));
			shared->setTokenSize(atoi((char *)sharedTokenSize));
		} else {
			shared->setTokenSize(atoi((char *)sharedSize));
			shared->setTokenRate(1);
			printf("Info: 'tokensize' attribute for channel '%s' undefined. Using "
				   "'size' as token "
				   "size; setting token rate to 1.\n",
				   sharedName);
		}
		if (sharedDelays != NULL) {
			shared->setInitialTokens(atoi((char *)sharedDelays));
		}

		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 1) {
				break;
			}
			readChannelDetails(shared, reader);
			ret = xmlTextReaderRead(reader);
		}
	} else {
		printf("Channel entry with incomplete attributes found: name and size "
			   "required\n");
	}
	free(sharedName);
	free(sharedSize);
	free(sharedTokenSize);
	free(sharedDelays);
}

void NetworkXMLReader::storeConnection(Network *network, xmlTextReaderPtr reader) {
	unsigned char *linkName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	if (linkName != NULL) {
#ifdef DEBUG
		printf("Link %s found\n", linkName);
#endif
		Link *link = network->addConnection((char *)linkName);
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 1) {
				break;
			}
			readConnectionDetails(link, reader);
			ret = xmlTextReaderRead(reader);
		}
	}
	free(linkName);
}

void NetworkXMLReader::readNetworkDetails(Network *network, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 1) {
		if (!strcmp((char *)name, "process")) {
			storeProcess(network, reader);
		} else if (!strcmp((char *)name, "sw_channel")) {
			storeChannel(network, reader);
		} else if (!strcmp((char *)name, "connection")) {
			storeConnection(network, reader);
		} else if ((xmlTextReaderNodeType(reader) == TEXTSKIP) ||
				   (xmlTextReaderNodeType(reader) == COMMENT)) {
		} else {
			printf("unknown name %s of type %i encountered\n", (char *)name,
				   xmlTextReaderNodeType(reader));
		}
	}
}

Network *NetworkXMLReader::storeNetwork(xmlTextReaderPtr reader) {
	Network *network = NULL;
	unsigned char *networkName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	if (networkName != NULL) {
#ifdef DEBUG
		printf("Network %s found\n", networkName);
#endif
		network = new Network();
		network->setName((char *)networkName);
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 0) {
				break;
			}
			readNetworkDetails(network, reader);
			ret = xmlTextReaderRead(reader);
		}
	}
	free(networkName);
	return network;
}

Network *NetworkXMLReader::readNetwork(const char *fileName) {
	Network *network = NULL;

	LIBXML_TEST_VERSION

	xmlTextReaderPtr reader = xmlReaderForFile(fileName, NULL, 0);

	if (reader != NULL) {
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			const unsigned char *name = xmlTextReaderConstName(reader);
			if (!strcmp((char *)name, "processnetwork")) {
				network = storeNetwork(reader);
				network->handleConfigurationPorts();
				network->connectDirectedGraph();
			} else if (xmlTextReaderNodeType(reader) == TEXTSKIP) {
			} else {
				printf("unknown name %s encountered\n", (char *)name);
			}
			ret = xmlTextReaderRead(reader);
		}
		xmlFreeTextReader(reader);
	} else {
		printf("Unable to open %s\n", fileName);
	}

	xmlCleanupParser();
	xmlMemoryDump();

	return network;
}
