#pragma once

#include "entity.h"

class Edge : public Entity {
  protected:
	Edge() {}
	~Edge() {}

  public:
	Edge(const Edge &obj);
	port *getOtherEndByHash(unsigned int hash);
};
