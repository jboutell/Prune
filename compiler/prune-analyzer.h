#pragma once

#include "controlFunctionWriter.h"
#include "network.h"

class Analyzer {
  private:
	typedef struct { // a structure that links DRPs and their attached Fifos (v)
		port *DRP;
		Vertex *v;
	} pfPair;
	Actor *findConfigurationActor(Network *network, Actor *a);
	Fifo *getFifoByName(Network *network, char *name);
	bool findPrecedence(Network *network, Actor *x, Actor *y);
	void findDynamicActors(Network *network, std::vector<Actor *> *dynamic);
	void findEmptyDCs(Network *network, dGraph *DPG);
	void formDCs(Network *network, dGraph *DPG, int index);
	void printDC(dC *DC, bool printPorts);
	void formDPGs(Network *network, std::vector<dGraph *> *DPGs);
	void formDCMaps(Network *network, char *dir);
	void checkDesignRule2(Network *network, dGraph *DPG, int index);
	void checkDesignRule3_1(Network *network, dGraph *DPG, int index);
	void checkDesignRule3_2(Network *network);
	void checkDesignRule4(Network *network);
	Actor *insertDummyActor(Network *network, Vertex *f1, Vertex *a1);
	port *discoverDRP(Network *network, Actor *x, Fifo *f, int direction);
	int compareVertexIndices(Network *network, Vertex *v, std::vector<pfPair *> *DRPs);
	void writeControlFunction(Network *network, dGraph *DPG, Actor *a, bool isX, char *dir);

  public:
	void doAnalysis(Network *network, char *dir);
};
