#pragma once

#include "genericXMLReader.h"
#include "mapping.h"

class MappingXMLReader : public GenericXMLReader {
  private:
	void storeTarget(Binding *link, xmlTextReaderPtr reader);
	void storeEndPoint(Mapping *mapping, Binding *link, xmlTextReaderPtr reader, int num);
	void readBindingDetails(Mapping *mapping, Binding *link, xmlTextReaderPtr reader);
	void storeBinding(Mapping *mapping, xmlTextReaderPtr reader);
	void readMappingDetails(Mapping *mapping, xmlTextReaderPtr reader);
	Mapping *storeMapping(xmlTextReaderPtr reader);
	void readTargetDetails(Binding *link, xmlTextReaderPtr reader);
	void storeOpenCL(Binding *link, xmlTextReaderPtr reader);

  public:
	MappingXMLReader(){};
	~MappingXMLReader(){};
	Mapping *readMapping(const char *fileName);
};
