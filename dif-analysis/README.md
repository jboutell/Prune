# Prune Analysis

This repository contains source codes that analysis PRUNE processing network in
DIFML format (`difml.xml`).

## Usage

The python script `cvt.py` is used to convert PRUNE procesing network to DIFML
format. It takes one argument, which is the processing network file name and
output one xml file called `difml.xml`.  **The limitation of this preliminary
convert** is that it cannot recognize the configuration actor of the PRUNE
model, because there is not special attribute associated to configuration
actor in pn.xml. A walkaround to this issue it to manually change the actor
type of the configuration actor from `io` to `control` so that the analyzer
can understand the processing network correctly. Otherwise the analyzer won't
work. To convert processing network to DIFML format, run the command: 

```bash
python ./cvt pn.xml
```

The required dependencies are listed in `requirements.txt` file, To
automatically install all required libraries, run:

```bash
pip install -r requirements.txt
```

*NOTE: remember to manually change the `actorType` attribute of the
configuration actor from "io" to "control" in the generated DIFML
document(file `difml.xml`).*

The analyzer is implemented in a single Java file and uses Dataflow Interchange
Format (DIF) as the only dependency. This dependency is included in
`lib/dif.jar`. To compile the analyzer, run:

```bash
./makeme
```

The generated java byte codes are stored in `build/` directory.

The analyzer takes two arguments: the first argument is th filename of the
DIFML document and the second argument is the filename of the control table,
which is in `csv` format. For users' convenience, we wrapped those commands into
one script call the `runme` command.

```bash
runme <DIFML file> <control table file>
```

After parsing the input DIFML document, the analyzer will first generate
DIFDoc, which includes a figure of the input dataflow model. The`graphviz`
package is required to generate the `.png` file.  Then the analyzer will check
the five **Desin Rules defined in the TSP paper**:

1. Linked port control rule
2. Balanced delay rule
3. Static Rate Port (SRP) connection rule
4. Single-sided dynamism rule
5. Encapsulation rule

After validating the input model against the five design rules, the analyzer
can divide the graph input multiple regions and output the analysis results.

In the `test` directory, we used the DPD application to demonstrate the
automatic process of checking design rules and performing region analysis.
`test01` demonstrates a valid PRUNE model that passes all design rule checks.
`test02` demonstrates a model that violates the *linked port control rule*.

