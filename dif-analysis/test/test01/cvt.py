#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#

import xmltodict


def main():
    with open('pn.xml') as fd:
        doc = xmltodict.parse(fd.read())
    network = doc["processnetwork"]
    process = network["process"]
    channel = network["sw_channel"]
    connection = network["connection"]

    dynamic = set()
    actor = dict()
    for item in process:
        actor[item['@name']] = item
        if '@dynamic' in item:
            dynamic.add(item['@name'])

    fifo = dict()
    for item in channel:
        fifo[item['@name']] = item

    difdoc = dict()
    difdoc['difml'] = dict()
    difdoc['difml']['@xmls'] = "http://www.ece.umd.edu/DIFML"
    difdoc['difml']['graph'] = list()
    impl = dict()
    impl["name"] = dict()
    impl["name"]['@val'] = doc["processnetwork"]['@name']
    impl["type"] = dict()
    impl["type"]['@val'] = "DIFGraph"
    graphlist = difdoc['difml']['graph']
    graph = dict()
    graph['topology'] = dict()
    nodes = list()
    graph['topology']['nodes'] = dict()
    graph['topology']['nodes']['node'] = nodes
    edges = list()
    graph['topology']['edges'] = dict()
    graph['topology']['edges']['edge'] = edges
    graph['implicitAttributes'] = impl
    graphlist.append(graph)

    for item in process:
        node = dict()
        node["implicitAttributes"] = dict()
        node["implicitAttributes"]["id"] = dict()
        node["implicitAttributes"]["id"]['@val'] = item['@name'].replace('-', '__')
        node["builtInAttributes"] = list()
        nodeWeight = dict()
        nodeWeight["nodeWeight"] = dict()
        nodeWeight["nodeWeight"]['@type'] = "DIFNodeWeight"
        node["builtInAttributes"].append(nodeWeight)
        node["userDefinedAttributes"] = dict()
        node["userDefinedAttributes"]["attribute"] = list()
        userList = node["userDefinedAttributes"]["attribute"]
        attr = dict()
        attr['@name'] = "actorType"
        attr['@type'] = 'String'
        if item['@type'] == 'io':
            attr['@val'] = 'io'
        elif item['@name'] in dynamic:
            attr['@val'] = 'dynamic'
        else:
            attr['@val'] = 'static'
        userList.append(attr)
        actor_ports = item['port']
        for p in actor_ports:
            if(p.__len__() != 2):
                break
            ap = dict()
            ap["@type"] = "Edge"
            ap["@name"] = p["@name"]
            if p["@type"] == "input":
                for c in connection:
                    snkActorName = c['target']['@name']
                    snkPortName = c['target']['port']['@name']
                    if snkPortName == ap["@name"] and snkActorName == item['@name']:
                        ap["@val"] = c['origin']['@name']
                        break
            else:
                for c in connection:
                    srcActorName = c['origin']['@name']
                    srcPortName = c['origin']['port']['@name']
                    if srcPortName == ap['@name'] and srcActorName == item['@name']:
                        ap["@val"] = c['target']['@name']
                        break
            userList.append(ap)
        nodes.append(node)

    for item in connection:
        edge = dict()
        edge["implicitAttributes"] = dict()
        impl = edge["implicitAttributes"]
        impl["id"] = dict()
        # impl["id"]['@val'] = item['@name'].replace('-', '__')
        impl['sourceId'] = dict()
        impl['sinkId'] = dict()
        srcName = item['origin']['@name']
        snkName = item['target']['@name']
        if srcName in actor and snkName in fifo:
            impl['sourceId']['@val'] = srcName.replace('-', '__')
            for it in connection:
                if it['origin']['@name'] == snkName:
                    impl['sinkId']['@val'] = it['target']['@name'].replace('-', '__')
                    impl["id"]['@val'] = snkName
                    connection.remove(it)
                    break

        if srcName in fifo and snkName in actor:
            impl['sinkId']['@val'] = snkName.replace('-', '__')
            for it in connection:
                if it['target']['@name'] == srcName:
                    impl['sourceId']['@val'] = it['origin']['@name'].replace('-', '__')
                    impl["id"]['@val'] = srcName
                    connection.remove(it)
                    break

        edge["builtInAttributes"] = dict()
        builtin = edge["builtInAttributes"]
        builtin["edgeWeight"] = dict()
        builtin["edgeWeight"]['@type'] = "DIFEdgeWeight"
        builtin["edgeWeight"]['@comsumption'] = "1"
        builtin["edgeWeight"]['@production'] = "1"
        builtin["edgeWeight"]['@delay'] = "0"
        edges.append(edge)

    with open('difml.xml', 'w') as fd:
        fd.write(xmltodict.unparse(difdoc, pretty=True))


if __name__ == "__main__":
    main()
