import dif.DIFHierarchy;
import dif.DIFLoader;
import dif.graph.DIFdoc;

import java.io.IOException;

public class Visualize {
    public static void main(String[] args) {
        DIFHierarchy hier = DIFLoader.loadDataflow(args[0]);
        generateDIFDoc(hier);
    }
    private static void generateDIFDoc(DIFHierarchy hier) {
        DIFdoc doc = new DIFdoc(hier);
        try {
            doc.toFile("index");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
