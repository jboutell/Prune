import dif.*;
import dif.graph.DIFdoc;
import dif.util.graph.Edge;
import dif.util.graph.Element;
import dif.util.graph.Node;
import dif.util.graph.analysis.AllPairShortestPathAnalysis;
import dif.util.graph.mapping.ToDoubleMapMapping;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class PruneAnalysis {

    final static boolean VERBOSE = false;

    public static void main(String[] argv) {
        Solver solver = new Solver();
        solver.solve(argv);
    }

    static class Solver {
        private DIFHierarchy hier;

        private void generateDIFDoc() {
            DIFdoc doc = new DIFdoc(hier);
            try {
                doc.toFile("index");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void solve(String[] argv) {
            if (argv.length != 2) {
                System.out.println("Two arguments expected");
                System.exit(-1);
            }
            String xmlFile = argv[0];
            String csvFile = argv[1];

            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";

            List<List<String>> table = new ArrayList<>();
            try {
                br = new BufferedReader(new FileReader(csvFile));
                while ((line = br.readLine()) != null) {
                    // use comma as separator
                    String[] row = line.split(cvsSplitBy);
                    for (int i = 0; i < row.length; i++) {
                        row[i] = row[i].replace(" ", "");
                    }
                    table.add(Arrays.asList(row));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            hier = DIFLoader.loadDIFMLs(xmlFile).get(0);
            generateDIFDoc();

            DIFGraph graph = (DIFGraph) hier.getGraph();

            Collection nodeList = graph.nodes();
            Collection edgeList = graph.edges();

            List<Node> dynamicList = new ArrayList<>();
            List<Node> ioList = new ArrayList<>();
            List<Node> ctlList = new ArrayList<>();

            for (Object n : nodeList) {
                Node node = (Node) n;
                String type = (String) graph.getAttribute((Element) n, "actorType").getValue();
                if (type.equals("dynamic")) {
                    String nodeName = graph.getName((Element) n);
                    System.out.println("[info] dynamic actor '" + nodeName + "' identified in difML");
                    dynamicList.add(node);
                }
                if (type.equals("io")) {
                    String nodeName = graph.getName((Element) n);
                    ioList.add(node);
                }
                if (type.equals("control")) {
                    String nodeName = graph.getName((Element) n);
                    System.out.println("[info] control actor '" + nodeName + "' identified in difML");
                    ctlList.add(node);
                }
            }

            Map<String, List<Set<String>>> checkList = new HashMap<>();
            for (Object c : ctlList) {
                Node node = (Node) c;
                for (Object o : graph.getAttributes(node)) {
                    DIFAttribute attr = (DIFAttribute) o;
                    if (attr.getType().equals("Edge")) {
                        String portID = attr.getName();
                        checkList.put(portID, new ArrayList<>());
                        System.out.println("[info] actor '" + graph.getName((Element) node) + "' port '" + portID + "' added to checklist");
                    }
                }
            }
            if (checkList.size() == 0) {
                System.out.println("[Warning] size of control port checklist is zero");
            }

            List<Set<Edge>> edgeSetList = new ArrayList<>();

            // for each dynamic actor
            for (Object n : dynamicList) {
                Node node = (Node) n;
                String controlOutput = "";
                Set<Edge> edgeSet = new HashSet<>();

                // find the control signal input port on the dynamic actor
                for (Object o : graph.getAttributes(node)) {
                    DIFAttribute attr = (DIFAttribute) o;
                    if (attr.getType().equals("Edge")) {
                        String portID = attr.getName();
                        String edgeID = (String) attr.getValue();

                        Set<String> ports = new HashSet<>();
                        ports.add(portID);

                        // find the edge connects to the control actor
                        for (Object e : edgeList) {
                            Edge edge = (Edge) e;
                            String edgeName = graph.getName(edge);
                            Node p = node;

                            if (edgeName.equals(edgeID) && ((Edge) e).sink() == p && graph.getAttribute(edge.source(), "actorType").getValue().equals("control")) {
                                edgeSet.add(edge);
                                // find the control output port
                                for (Object o2 : graph.getAttributes(((Edge) e).source())) {
                                    DIFAttribute attr2 = (DIFAttribute) o2;
                                    if (attr2.getValue().equals(graph.getName(edge))) {
                                        controlOutput = attr2.getName();
                                        break;
                                    }
                                }
                            }
                            if (edgeName.equals(edgeID) && ((Edge) e).sink() == p) {
                                while (!graph.getAttribute(edge.source(), "actorType").getValue().equals("dynamic")) {
                                    p = edge.source();
                                    if (graph.getAttribute(p, "actorType").getValue().equals("io")) {
                                        break;
                                    }
                                    if (graph.getAttribute(p, "actorType").getValue().equals("control")) {
                                        break;
                                    }

                                    for (Object e1 : edgeList) {
                                        Edge edge1 = (Edge) e1;
                                        if (((Edge) e1).sink() == p) {
                                            edge = edge1;
                                        }
                                    }
                                }
                                if (graph.getAttribute(edge.source(), "actorType").getValue().equals("control") || graph.getAttribute(edge.source(), "actorType").getValue().equals("io")) {
                                    continue;
                                }
                                for (Object o2 : graph.getAttributes(edge.source())) {
                                    DIFAttribute attr2 = (DIFAttribute) o2;
                                    if (attr2.getValue().equals(graph.getName(edge))) {
                                        ports.add(attr2.getName());
                                        break;
                                    }
                                }
                                break;
                            }
                            if (edgeName.equals(edgeID) && ((Edge) e).source() == p) {
                                while (!graph.getAttribute(edge.sink(), "actorType").getValue().equals("dynamic")) {
                                    p = edge.sink();
                                    if (graph.getAttribute(p, "actorType").getValue().equals("io")) {
                                        break;
                                    }
                                    if (graph.getAttribute(p, "actorType").getValue().equals("control")) {
                                        break;
                                    }

                                    for (Object e1 : edgeList) {
                                        Edge edge1 = (Edge) e1;
                                        if (((Edge) e1).source() == p) {
                                            edge = edge1;
                                        }
                                    }
                                }
                                if (graph.getAttribute(edge.source(), "actorType").getValue().equals("control") || graph.getAttribute(edge.source(), "actorType").getValue().equals("io")) {
                                    continue;
                                }
                                for (Object o2 : graph.getAttributes(edge.sink())) {
                                    DIFAttribute attr2 = (DIFAttribute) o2;
                                    if (attr2.getValue().equals(graph.getName(edge))) {
                                        ports.add(attr2.getName());
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        if (ports.size() > 1 && !controlOutput.equals("")) {
                            checkList.get(controlOutput).add(ports);
                        }
                    }
                }
            }

            connectingSubchainRule(graph, dynamicList);
            //encapsulationRule(graph, dynamicList);
            singleSidedDynamismRule(graph, dynamicList);
            linkedPortControlRule(table, checkList);
            balancedDelayRule(graph, edgeSetList);
            dataPortConnectionRule(graph, ctlList);
            regionAnalysis(graph, dynamicList, ioList, ctlList);
        }

        private void connectingSubchainRule(DIFGraph graph, List<Node> dynamicList) {
            System.out.println("[info] Starting to check connecting subchain rule ...");
            for (Node dnode : dynamicList) {
                List<Node> succ = (List<Node>) graph.successors(dnode);
                if (succ.size() > 0) {
                    int cnt = explore(graph, dnode).size();
                    if (cnt > 2) {
                        System.out.println("[error] connecting subchain rule verification failed on the chain" +
                                " that associates with dynamic actor " + graph.getName(dnode));
                        break;
                    }
                }
            }
            System.out.println("[info] --> passed");
        }

        private Set<Node> explore(DIFGraph graph, Node s) {
            Set<Node> res = new HashSet<>();
            Queue<Node> q = new LinkedList<>();
            q.add(s);
            while (!q.isEmpty()) {
                Node current = q.poll();
                if (graph.getAttribute(current, "actorType").getValue().toString().equals("dynamic")) {
                    res.add(current);
                }
                List<Node> succ = (List<Node>) graph.successors(current);
                q.addAll(succ);
            }
            return res;
        }

        private void singleSidedDynamismRule(DIFGraph graph, List<Node> dynamicList) {
            System.out.println("[info] Starting to check single-sided dynamism rule ...");
            for (Node dynnode : dynamicList) {
                List<Node> preds = (List<Node>) graph.predecessors(dynnode);
                List<Node> succs = (List<Node>) graph.successors(dynnode);
                String nodename = graph.getName(dynnode);

                boolean pflag = false, sflag = false;
                for (Node p : preds) {
                    String at = graph.getAttribute(p, "actorType").getValue().toString();
                    if (at.equals("static")) {
                        pflag = true;
                        break;
                    }
                }

                for (Node s : succs) {
                    String at = graph.getAttribute(s, "actorType").getValue().toString();
                    if (at.equals("static")) {
                        sflag = true;
                        break;
                    }
                }

                if (sflag && pflag) {
                    System.out.println(" violated in node: " + nodename);
                }
            }
            System.out.println("[info] --> passed");
        }

        private void dataPortConnectionRule(DIFGraph graph, List<Node> ctlList) {
            System.out.println("[info] Starting to check data port connection rule ...");
            boolean pass = true;
            DIFGraph localGraph = (DIFGraph) graph.clone();
            for (Object n : localGraph.nodes()) {
                Node node = (Node) n;
                String type = (String) graph.getAttribute(node, "actorType").getValue();
                if (type.equals("dynamic") || type.equals("io")) {
                    graph.removeNode(node);
                }
            }

            Collection col = graph.connectedComponents();
            int cnt = 0;
            for (Object o : col) {
                ArrayList list = (ArrayList) o;
                Set set = new HashSet(list);
                for (Node n : ctlList) {
                    if (set.contains(n) && list.size() > 1) {
                        pass = false;

                        System.out.printf("[error] violated in node: ", ++cnt);
                        for (Object node : list) {
                            System.out.printf("%s ", graph.getName((Element) node));
                        }
                        System.out.println();
                    }
                }
            }

            if (pass) {
                System.out.println("[info] --> passed");
            }
        }

        private void encapsulationRule(DIFGraph graph, List<Node> dynamicList) {
            System.out.println("[info] Starting to check encapsulation rule ...");
            Map<Edge, Double> map = new HashMap<>();
            for (Object obj : graph.edges()) {
                Edge edge = (Edge) obj;
                map.put(edge, 1.0);
            }

            List<Integer> dynLabels = new ArrayList<>();
            for (Node dynnode : dynamicList) {
                dynLabels.add(graph.nodeLabel(dynnode));
            }

            AllPairShortestPathAnalysis analyzer = new AllPairShortestPathAnalysis(graph, new ToDoubleMapMapping(map));
            double[][] connectivity = analyzer.shortestPathMatrix();

            for (Object obj : graph.nodes()) {
                Node node = (Node) obj;
                String actoryType = graph.getAttribute(node, "actorType").getValue().toString();
                if (actoryType.equals("static")) {
                    int currentLable = graph.nodeLabel(node);
                    Set<Integer> headSet = new HashSet<>();
                    Set<Integer> tailSet = new HashSet<>();

                    for (int dl : dynLabels) {
                        if (connectivity[dl][currentLable] < 1000000) {
                            headSet.add(dl);
                        }
                        if (connectivity[currentLable][dl] < 1000000) {
                            tailSet.add(dl);
                        }
                    }

                    List<Node> preds = (List<Node>) graph.predecessors(node);
                    List<Node> succs = (List<Node>) graph.successors(node);

                    for (Node nn : preds) {
                        if (!graph.getAttribute(nn, "actorType").getValue().toString().equals("static"))
                            continue;
                        int nlbl = graph.nodeLabel(nn);
                        for (int h : headSet) {
                            if (connectivity[h][nlbl] >= 1000000) {
                                System.out.println("Node " + graph.getName(nn) + " violated");
                                System.exit(-1);
                            }
                        }
                        for (int t : tailSet) {
                            if (connectivity[nlbl][t] >= 1000000) {
                                System.out.println("Node " + graph.getName(nn) + " violated");
                                System.exit(-1);
                            }
                        }
                    }

                    for (Node nn : succs) {
                        if (!graph.getAttribute(nn, "actorType").getValue().toString().equals("static"))
                            continue;
                        int nlbl = graph.nodeLabel(nn);
                        for (int h : headSet) {
                            if (connectivity[h][nlbl] >= 100000) {
                                System.out.println("Node " + graph.getName(nn) + " violated");
                                System.exit(-1);
                            }
                        }
                        for (int t : tailSet) {
                            if (connectivity[nlbl][t] >= 100000) {
                                System.out.println("Node " + graph.getName(nn) + " violated");
                                System.exit(-1);
                            }
                        }
                    }
                }
            }

            System.out.println("[info] --> passed");
        }

        private void balancedDelayRule(DIFGraph graph, List<Set<Edge>> edgeSetList) {
            System.out.println("[info] Starting to check balanced delay rule ...");
            boolean pass = true;
            for (Set<Edge> edgeSet : edgeSetList) {
                boolean isFirst = true;
                int delay = 0;
                for (Edge edge : edgeSet) {
                    DIFEdgeWeight weight = (DIFEdgeWeight) edge.getWeight();
                    if (isFirst) {
                        delay = (int) weight.getDelay();
                        isFirst = false;
                    } else {
                        if (delay != (int) weight.getDelay()) {
                            pass = false;
                            System.out.print("[error] violated in edges: ");
                            for (Edge e : edgeSet) {
                                System.out.print(", " + graph.getName(e));
                            }
                            System.out.println();
                            break;
                        }
                    }
                }
            }
            if (pass) {
                System.out.println("[info] --> passed");
            }
        }

        private void linkedPortControlRule(List<List<String>> table, Map<String, List<Set<String>>> checkList) {
            System.out.println("[info] Starting to check linked port control rule ...");
            boolean pass = true;
            for (String controlPort : checkList.keySet()) {
                List<Set<String>> dynamicSetList = checkList.get(controlPort);
                for (Set<String> dynamicPorts : dynamicSetList) {
                    boolean isFirst = true;
                    int index = 0;
                    for (int i = 0; i < table.size(); ++i) {
                        if (table.get(i).get(0).equals(controlPort)) {
                            List<String> list = table.get(i);
                            if (VERBOSE) {
                                System.out.println(" Checking port " + table.get(i).get(0));
                            }
                            for (int j = 0; j < list.size(); ++j) {
                                String port = table.get(0).get(j);
                                for (String setPort : dynamicPorts) {
                                    if (setPort.equals(port)) {
                                        String listJ = list.get(j);
                                        if (VERBOSE) {
                                            System.out.println("  against " + listJ);
                                        }
                                        if (isFirst) {
                                            index = Integer.valueOf(listJ);
                                            isFirst = false;
                                        } else {
                                            if (Integer.valueOf(listJ) != index) {
                                                pass = false;
                                                System.out.print("[error] violated in ports: " + controlPort);
                                                for (String p : dynamicPorts) {
                                                    System.out.print(", " + p);
                                                }
                                                System.out.println();
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (pass) {
                System.out.println("[info] --> passed");
            }
        }

        private void regionAnalysis(DIFGraph graph, List<Node> dynamicList, List<Node> ioList, List<Node> ctlList) {
            System.out.println("[info] Connected component analysis results:");
            for (Node n : dynamicList) {
                graph.removeNode(n);
            }
            for (Node n : ioList) {
                graph.removeNode(n);
            }
            for (Node n : ctlList) {
                graph.removeNode(n);
            }

            Collection col = graph.connectedComponents();
            int cnt = 0;
            for (Object o : col) {
                ArrayList list = (ArrayList) o;
                System.out.printf("Region %d: ", ++cnt);
                for (Object n : list) {
                    System.out.printf("%s ", graph.getName((Element) n));
                }
                System.out.println();
            }
        }
    }
}
