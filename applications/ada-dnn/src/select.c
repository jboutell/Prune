#include "dnn.h"
#include "select.h"
#include <stdlib.h>
#include <string.h>

void selectInit(select_data_t *data) {
	char selectFileName[256];
	data->file = NULL;
	sprintf(selectFileName, "%s/%s.bin", getenv("HOME"), data->fn);
	data->iteration = 0;
	data->file = fopen(selectFileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", selectFileName);
		return;
	}
}

void *selectFire(void *p) {
	select_data_t *data = (select_data_t *) p;
	#ifdef DBGPRINT
	printf("actor select fires\n");
	#endif
	// read image and add PAD_NUM padding on each side of image
	int writeOffset = 0;
	int readOffset = 0;
	char cenable;
	int enable;
	int retval = fread(&cenable, sizeof(char), 1, data->file);
	if (retval != 1) {
		printf("Source %s depleted\n", data->fn);
	}
	enable = (int) cenable;
	int *wbufout3 = (int *) fifoWriteStart(data->shared->outputs[2]);
	wbufout3[0] = enable;
	fifoWriteEnd(data->shared->outputs[2]);
	float *rbufin1 = (float *) fifoReadStart(data->shared->inputs[0]);
	if (enable == 1) {
		float *wbufout1 = (float *) fifoWriteStart(data->shared->outputs[0]);
		for (int r = 0; r < REPEAT; r++) {
			for (int c = 0; c < CHANNELS; c++) {
				cl_float *fifop = &wbufout1[writeOffset + c*PATCH1SQPAD];
				memset(fifop, 0, 2*(PATCH1 + 2*PAD_NUM)*sizeof(cl_float));
				for (int i = PAD_NUM; i < (PATCH1 + PAD_NUM); i++) {
					memset(&fifop[i * (PATCH1+2*PAD_NUM)], 0, PAD_NUM*sizeof(cl_float));
					memcpy(&fifop[i * (PATCH1+2*PAD_NUM) + PAD_NUM], &rbufin1[readOffset], sizeof(cl_float)*PATCH1);
					memset(&fifop[i * (PATCH1+2*PAD_NUM) + PAD_NUM + PATCH1], 0, PAD_NUM*sizeof(cl_float));
					readOffset += PATCH1;
				}
				memset(&fifop[(PATCH1 + PAD_NUM) * (PATCH1+2*PAD_NUM)], 0, 2*(PATCH1 + 2*PAD_NUM)*sizeof(cl_float));
			}
			writeOffset += TOKEN1SIZE;
		}
		fifoWriteEnd(data->shared->outputs[0]);
	} else {
		float *wbufout2 = (float *) fifoWriteStart(data->shared->outputs[1]);
		wbufout2[0] = -1.0;
		fifoWriteEnd(data->shared->outputs[1]);
	}
	fifoReadEnd(data->shared->inputs[0]);

	return p;
}

void selectFinish(select_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

