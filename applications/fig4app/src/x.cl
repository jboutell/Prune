#include "fig4app.h"
#include "ports.h"

#define x_in1_type int*
#define x_in2_type float*
#define x_out1_type float*
#define x_out2_type float*
#define x_out3_type float*
#define x_out4_type float*

__kernel void x(
	__global x_input_0_type restrict x_input_0_name,
	__global x_input_1_type restrict x_input_1_name,
	__global x_output_0_type restrict x_output_0_name,
	__global x_output_1_type restrict x_output_1_name,
	__global x_output_2_type restrict x_output_2_name,
	__global x_output_3_type restrict x_output_3_name) {

	if (x_in1[0] == 1) {
		x_out1[0] = x_in2[0];
		x_out2[0] = x_in2[0];
	} else if (x_in1[0] == 2) {
		x_out3[0] = x_in2[0];
	} else if (x_in1[0] == 3) {
		x_out4[0] = x_in2[0];
	}
}
