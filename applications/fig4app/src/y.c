#include "common.h"
#include "ports.h"

void *yIO(void *p) {
	io_data_t *data = (io_data_t *) p;

	int n_ind = data->cValue;

	data->inputEnabled[Y_IN1] = 1;
	data->inputEnabled[Y_IN2] = (n_ind == 1);
	data->inputEnabled[Y_IN3] = (n_ind == 2);
	data->inputEnabled[Y_IN4] = (n_ind == 3);
	data->outputEnabled[Y_OUT1] = 1;

	return p;
}
