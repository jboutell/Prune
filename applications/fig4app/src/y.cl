#include "fig4app.h"
#include "ports.h"

#define y_in1_type int*
#define y_in2_type float*
#define y_in3_type float*
#define y_in4_type float*
#define y_out1_type float*

__kernel void y(
	__global y_input_0_type restrict y_input_0_name,
	__global y_input_1_type restrict y_input_1_name,
	__global y_input_2_type restrict y_input_2_name,
	__global y_input_3_type restrict y_input_3_name,
	__global y_output_0_type restrict y_output_0_name) {

	if (y_in1[0] == 1) {
		y_out1[0] = y_in2[0];
	} else if (y_in1[0] == 2) {
		y_out1[0] = y_in3[0];
	} else if (y_in1[0] == 3) {
		y_out1[0] = y_in4[0];
	}
}
