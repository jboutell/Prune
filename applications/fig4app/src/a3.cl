#include "fig4app.h"
#include "ports.h"

#define a3_in1_type float*
#define a3_out1_type float*

__kernel void a3(
	__global a3_input_0_type restrict a3_input_0_name,
	__global a3_output_0_type restrict a3_output_0_name) {
	a3_out1[0] = a3COEFF * a3_in1[0];
}
