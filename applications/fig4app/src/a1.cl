#include "fig4app.h"
#include "ports.h"

#define a1_in1_type float*
#define a1_out1_type float*

__kernel void a1(
	__global a1_input_0_type restrict a1_input_0_name,
	__global a1_output_0_type restrict a1_output_0_name) {
	a1_out1[0] = a1COEFF * a1_in1[0];
}
