#include "fig4app.h"
#include "ports.h"

#define a4_in1_type float*
#define a4_out1_type float*

__kernel void a4(
	__global a4_input_0_type restrict a4_input_0_name,
	__global a4_output_0_type restrict a4_output_0_name) {
	a4_out1[0] = a4COEFF * a4_in1[0];
}
