#include "common.h"
#include "ports.h"

void *xIO(void *p) {
	io_data_t *data = (io_data_t *) p;

	int n_ind = data->cValue;

	data->inputEnabled[X_IN1] = 1;
	data->inputEnabled[X_IN2] = 1;
	data->outputEnabled[X_OUT1] = (n_ind == 1);
	data->outputEnabled[X_OUT2] = (n_ind == 1);
	data->outputEnabled[X_OUT3] = (n_ind == 2);
	data->outputEnabled[X_OUT4] = (n_ind == 3);

	return p;
}
