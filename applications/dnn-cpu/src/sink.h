#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	int count;
	int length;
	shared_t *shared;
	char const *fn;
	int iteration;
	int repetitions;
	float *coeff_l4;
	float *coeff_l5;
} sink_data_t;

void sinkInit(sink_data_t *data);
void *sinkFire(void *p);
void sinkFinish(sink_data_t *data);

