#pragma once

#include "common.h"

typedef struct {
	shared_t *shared;
} a2_data_t;

void a2Init(a2_data_t *data);
void *a2Fire(void *p);
void a2Finish(a2_data_t *data);
