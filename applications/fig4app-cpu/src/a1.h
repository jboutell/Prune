#pragma once

#include "common.h"

typedef struct {
	shared_t *shared;
} a1_data_t;

void a1Init(a1_data_t *data);
void *a1Fire(void *p);
void a1Finish(a1_data_t *data);
