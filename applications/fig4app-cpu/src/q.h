#pragma once

#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	shared_t *shared;
	char const *fn;
	int iteration;
} q_data_t;

void qInit(q_data_t *data);
void *qFire(void *p);
void qFinish(q_data_t *data);
