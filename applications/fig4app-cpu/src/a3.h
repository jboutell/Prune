#pragma once

#include "common.h"

typedef struct {
	shared_t *shared;
} a3_data_t;

void a3Init(a3_data_t *data);
void *a3Fire(void *p);
void a3Finish(a3_data_t *data);
