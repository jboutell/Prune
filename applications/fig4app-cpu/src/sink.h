#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	shared_t *shared;
	char const *fn;
	int iteration;
	int repetitions;
} sink_data_t;

void sinkInit(sink_data_t *data);
void *sinkFire(void *p);
void sinkFinish(sink_data_t *data);
