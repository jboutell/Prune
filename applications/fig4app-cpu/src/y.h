#pragma once

#include <stdio.h>
#include "common.h"

typedef struct {
	shared_t *shared;
} y_data_t;

void yInit(y_data_t *data);
void *yFire(void *p);
void yFinish(y_data_t *data);
