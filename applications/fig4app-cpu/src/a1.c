#include "fig4app.h"
#include "a1.h"

void a1Init(a1_data_t *data) {
}

void a1Finish(a1_data_t *data) {
}

void *a1Fire(void *p) {
	a1_data_t *data = (a1_data_t *) p;
	cl_float *input = (cl_float *) fifoReadStart(data->shared->inputs[0]);
	cl_float *output = (cl_float *) fifoWriteStart(data->shared->outputs[0]);
	#ifdef DBGPRINT
	printf("actor a1 fires\n");
	#endif

	output[0] = a1COEFF * input[0];

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}
