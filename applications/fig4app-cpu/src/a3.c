#include "fig4app.h"
#include "a3.h"

void a3Init(a3_data_t *data) {
}

void a3Finish(a3_data_t *data) {
}

void *a3Fire(void *p) {
	a3_data_t *data = (a3_data_t *) p;
	cl_float *input = (cl_float *) fifoReadStart(data->shared->inputs[0]);
	cl_float *output = (cl_float *) fifoWriteStart(data->shared->outputs[0]);
	#ifdef DBGPRINT
	printf("actor a3 fires\n");
	#endif

	output[0] = a3COEFF * input[0];

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}
