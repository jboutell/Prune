#include "fig4app.h"
#include "a4.h"

void a4Init(a4_data_t *data) {
}

void a4Finish(a4_data_t *data) {
}

void *a4Fire(void *p) {
	a4_data_t *data = (a4_data_t *) p;
	cl_float *input = (cl_float *) fifoReadStart(data->shared->inputs[0]);
	cl_float *output = (cl_float *) fifoWriteStart(data->shared->outputs[0]);
	#ifdef DBGPRINT
	printf("actor a4 fires\n");
	#endif

	output[0] = a4COEFF * input[0];

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}
