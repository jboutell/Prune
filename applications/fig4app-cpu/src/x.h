#pragma once

#include <stdio.h>
#include "common.h"

typedef struct {
	shared_t *shared;
} x_data_t;

void xInit(x_data_t *data);
void *xFire(void *p);
void xFinish(x_data_t *data);
