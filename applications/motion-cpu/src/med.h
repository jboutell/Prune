#include <stdio.h>
#include "common.h"

typedef struct {
	shared_t *shared;
} med_data_t;

int medInit(med_data_t *data);
void *medFire(void *p);
void medFinish(med_data_t *data);

