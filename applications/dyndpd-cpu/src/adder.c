#include "adder.h"
#include "ports.h"

#define DPD_Adder_ic_lo -0.004622196778655052
#define DPD_Adder_qc_lo -0.004829585552215576

int adderInit(adder_data_t *data) {
	return 0;
}

void adderFinish(adder_data_t *data) {
}
	
void* adderFire(void *p) {
	adder_data_t *data = (adder_data_t *) p;

	int *n_c_buffer = (int *) fifoReadStart(data->shared->inputs[ADDER_N_C]);
	int n_ind = n_c_buffer[0] >> 8;
	int c_ind = n_c_buffer[0] & 0xFF;
	fifoReadEnd(data->shared->inputs[ADDER_N_C]);

	float ivect_tmp[VALID_DATA_SIZE];
	float qvect_tmp[VALID_DATA_SIZE];

	for(int j = 0; j < VALID_DATA_SIZE; j ++) {
		ivect_tmp[j] = DPD_Adder_ic_lo;
		qvect_tmp[j] = DPD_Adder_qc_lo;
	}

	if (n_ind == 5) {
		float *iInput5 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN5]);
		float *qInput5 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN5]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput5[j];
			qvect_tmp[j] += qInput5[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_IN5]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_IN5]);
	}
	if (n_ind >= 4) {
		float *iInput4 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN4]);
		float *qInput4 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN4]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput4[j];
			qvect_tmp[j] += qInput4[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_IN4]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_IN4]);
	}
	if (n_ind >= 3) {
		float *iInput3 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN3]);
		float *qInput3 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN3]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput3[j];
			qvect_tmp[j] += qInput3[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_IN3]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_IN3]);
	}
	if (n_ind >= 2) {
		float *iInput2 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN2]);
		float *qInput2 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN2]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput2[j];
			qvect_tmp[j] += qInput2[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_IN2]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_IN2]);
	}
	if (n_ind >= 1) {
		float *iInput1 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN1]);
		float *qInput1 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN1]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j];
			qvect_tmp[j] += qInput1[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_IN1]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_IN1]);
	}

	if (c_ind == 5) {
		float *iInputA = (float *) fifoReadStart(data->shared->inputs[ADDER_I_INA]);
		float *qInputA = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_INA]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInputA[j];
			qvect_tmp[j] += qInputA[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_INA]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_INA]);
	}
	if (c_ind >= 4) {
		float *iInput9 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN9]);
		float *qInput9 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN9]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput9[j];
			qvect_tmp[j] += qInput9[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_IN9]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_IN9]);
	}
	if (c_ind >= 3) {
		float *iInput8 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN8]);
		float *qInput8 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN8]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput8[j];
			qvect_tmp[j] += qInput8[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_IN8]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_IN8]);
	}
	if (c_ind >= 2) {
		float *iInput7 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN7]);
		float *qInput7 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN7]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput7[j];
			qvect_tmp[j] += qInput7[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_IN7]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_IN7]);
	}
	if (c_ind >= 1) {
		float *iInput6 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN6]);
		float *qInput6 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN6]);
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput6[j];
			qvect_tmp[j] += qInput6[j];
		}
		fifoReadEnd(data->shared->inputs[ADDER_I_IN6]);
		fifoReadEnd(data->shared->inputs[ADDER_Q_IN6]);
	}

	float *iOutput = (float *) fifoWriteStart(data->shared->outputs[ADDER_I_OUT]);
	float *qOutput = (float *) fifoWriteStart(data->shared->outputs[ADDER_Q_OUT]);
	for(int j = 0; j < VALID_DATA_SIZE; j ++) {
		iOutput[j] = ivect_tmp[j];
		qOutput[j] = qvect_tmp[j];
	}
	fifoWriteEnd(data->shared->outputs[ADDER_I_OUT]);
	fifoWriteEnd(data->shared->outputs[ADDER_Q_OUT]);
	return p;
}
