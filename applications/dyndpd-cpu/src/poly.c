#include "poly.h"
#include "ports.h"

int polyInit(poly_data_t *data) {
	return 0;
}

void polyFinish(poly_data_t *data) {
}
	
void *polyFire(void *p) {
	poly_data_t *data = (poly_data_t *) p;

	int *n_c_buffer = (int *) fifoReadStart(data->shared->inputs[POLY_N_C]);
	int n_ind = n_c_buffer[0] >> 8;
	int c_ind = n_c_buffer[0] & 0xFF;
	fifoReadEnd(data->shared->inputs[POLY_N_C]);

	float polytab[NORM][TOKEN_SIZE];
	float abs_sqr_o;

	cl_float *iInput = (cl_float *) fifoReadStart(data->shared->inputs[POLY_I_IN]);
	cl_float *qInput = (cl_float *) fifoReadStart(data->shared->inputs[POLY_Q_IN]);

	for(int j = 0; j < TOKEN_SIZE; j++) {
		abs_sqr_o = iInput[j] * iInput[j] + qInput[j] * qInput[j];
		polytab[0][j] = 1.0;
		for(int i = 1; i < NORM; i ++) {
			polytab[i][j] = polytab[i - 1][j] * abs_sqr_o;
		}
	}

	if (n_ind == 5) {
		float *iOutput5 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT5]);
		float *qOutput5 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT5]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput5[j] = iInput[j] * polytab[4][j];
			qOutput5[j] = qInput[j] * polytab[4][j];
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUT5]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUT5]);
	}
	if (n_ind >= 4) {
		float *iOutput4 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT4]);
		float *qOutput4 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT4]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput4[j] = iInput[j] * polytab[3][j];
			qOutput4[j] = qInput[j] * polytab[3][j];
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUT4]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUT4]);
	}
	if (n_ind >= 3) {
		float *iOutput3 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT3]);
		float *qOutput3 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT3]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput3[j] = iInput[j] * polytab[2][j];
			qOutput3[j] = qInput[j] * polytab[2][j];
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUT3]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUT3]);
	}
	if (n_ind >= 2) {
		float *iOutput2 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT2]);
		float *qOutput2 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT2]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput2[j] = iInput[j] * polytab[1][j];
			qOutput2[j] = qInput[j] * polytab[1][j];
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUT2]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUT2]);
	}
	if (n_ind >= 1) {
		float *iOutput1 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT1]);
		float *qOutput1 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT1]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[j] = iInput[j] * polytab[0][j];
			qOutput1[j] = qInput[j] * polytab[0][j];
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUT1]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUT1]);
	}

	if (c_ind == 5) {
		float *iOutputA = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUTA]);
		float *qOutputA = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUTA]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutputA[j] = iInput[j] * polytab[4][j];
			qOutputA[j] = -(qInput[j] * polytab[4][j]);
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUTA]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUTA]);
	}
	if (c_ind >= 4) {
		float *iOutput9 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT9]);
		float *qOutput9 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT9]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput9[j] = iInput[j] * polytab[3][j];
			qOutput9[j] = -(qInput[j] * polytab[3][j]);
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUT9]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUT9]);
	}
	if (c_ind >= 3) {
		float *iOutput8 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT8]);
		float *qOutput8 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT8]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput8[j] = iInput[j] * polytab[2][j];
			qOutput8[j] = -(qInput[j] * polytab[2][j]);
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUT8]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUT8]);
	}
	if (c_ind >= 2) {
		float *iOutput7 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT7]);
		float *qOutput7 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT7]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput7[j] = iInput[j] * polytab[1][j];
			qOutput7[j] = -(qInput[j] * polytab[1][j]);
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUT7]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUT7]);
	}
	if (c_ind >= 1) {
		float *iOutput6 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT6]);
		float *qOutput6 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT6]);
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput6[j] = iInput[j] * polytab[0][j];
			qOutput6[j] = -(qInput[j] * polytab[0][j]);
		}
		fifoWriteEnd(data->shared->outputs[POLY_I_OUT6]);
		fifoWriteEnd(data->shared->outputs[POLY_Q_OUT6]);
	}

	fifoReadEnd(data->shared->inputs[POLY_I_IN]);
	fifoReadEnd(data->shared->inputs[POLY_Q_IN]);

	return p;
}
