#ifndef fir6_H
#define fir6_H

#include "common.h"

typedef struct {
	shared_t *shared;
} fir6_data_t;

int fir6Init(fir6_data_t *data);
void *fir6Fire(void *p);
void fir6Finish(fir6_data_t *data);

#endif
