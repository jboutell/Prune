#ifndef POLY_H
#define POLY_H

#include "common.h"
#include "dyndpd.h"

typedef struct {
	int _FSM_state;
	int n_ind;
	int c_ind;
	float ivect[TOKEN_SIZE];
	float qvect[TOKEN_SIZE];
	float polytab[NORM][TOKEN_SIZE];
	shared_t *shared;
} poly_data_t;

int polyInit(poly_data_t *data);
void *polyFire(void *p);
void polyFinish(poly_data_t *data);

#endif
