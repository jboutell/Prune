#ifndef fir1_H
#define fir1_H

#include "common.h"

typedef struct {
	shared_t *shared;
} fir1_data_t;

int fir1Init(fir1_data_t *data);
void *fir1Fire(void *p);
void fir1Finish(fir1_data_t *data);

#endif
