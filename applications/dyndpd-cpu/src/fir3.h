#ifndef fir3_H
#define fir3_H

#include "common.h"

typedef struct {
	shared_t *shared;
} fir3_data_t;

int fir3Init(fir3_data_t *data);
void *fir3Fire(void *p);
void fir3Finish(fir3_data_t *data);

#endif
