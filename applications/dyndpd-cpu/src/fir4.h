#ifndef fir4_H
#define fir4_H

#include "common.h"

typedef struct {
	shared_t *shared;
} fir4_data_t;

int fir4Init(fir4_data_t *data);
void *fir4Fire(void *p);
void fir4Finish(fir4_data_t *data);

#endif
