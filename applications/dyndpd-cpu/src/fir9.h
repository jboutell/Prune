#ifndef fir9_H
#define fir9_H

#include "common.h"

typedef struct {
	shared_t *shared;
} fir9_data_t;

int fir9Init(fir9_data_t *data);
void *fir9Fire(void *p);
void fir9Finish(fir9_data_t *data);

#endif
