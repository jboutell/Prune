#include "dyndpd.h"
#include "fir8.h"
#include "ports.h"

int fir8Init(fir8_data_t *data) {
	return 0;
}

void fir8Finish(fir8_data_t *data) {
}

void *fir8Fire(void *p) {

	fir8_data_t *data = (fir8_data_t *) p;
	float *iTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR8_I_IN]);
	float *qTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR8_Q_IN]);
	float *iTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR8_I_OUT]);
	float *qTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR8_Q_OUT]);

	const float fir_ic[NUM_TAPS] = 
		{-0.010361481457948685, -0.0004938115598633885, 0.004675863776355982, -0.003926571924239397, 0.0046980371698737144, -0.006110482849180698, 0.006143227219581604, -0.004450784996151924, 0.002773092593997717, -0.0021631026174873114}; 
	const float fir_qc[NUM_TAPS] = 
		{0.003214986063539982, -0.004554244689643383, 0.0044721211306750774, -0.0006862758309580386, -0.0020631044171750546, 0.002121124416589737, -0.0013605181593447924, -0.0001483698288211599, 0.001545978942885995, -0.001356405089609325}; 

	for (int j = 0; j < VALID_DATA_SIZE; j++) {
		float io_tmp = 0.0;
		float qo_tmp = 0.0;
		for (int t = 0; t < NUM_TAPS; t++) {
			float tmp_i_in = iTokenIn[j+t];
			float tmp_q_in = qTokenIn[j+t];
			float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
			float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
			io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
			qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
		}
		iTokenOut[j] = io_tmp;
		qTokenOut[j] = qo_tmp;
	}
	fifoReadEnd(data->shared->inputs[FIR8_I_IN]);
	fifoReadEnd(data->shared->inputs[FIR8_Q_IN]);
	fifoWriteEnd(data->shared->outputs[FIR8_I_OUT]);
	fifoWriteEnd(data->shared->outputs[FIR8_Q_OUT]);
	return p;
}

