#include "dyndpd.h"
#include "fir4.h"
#include "ports.h"

int fir4Init(fir4_data_t *data) {
	return 0;
}

void fir4Finish(fir4_data_t *data) {
}

void *fir4Fire(void *p) {

	fir4_data_t *data = (fir4_data_t *) p;
	float *iTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR4_I_IN]);
	float *qTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR4_Q_IN]);
	float *iTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR4_I_OUT]);
	float *qTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR4_Q_OUT]);

	const float fir_ic[NUM_TAPS] = 
		{-0.02557271718978882, 0.011701775714755058, -0.005951288156211376, 0.0011874770279973745, 0.00007474123412976041, 0.0016221854602918029, -0.003584664547815919, 0.005168240517377853, -0.00659256661310792, 0.006709362845867872}; 
	const float fir_qc[NUM_TAPS] = 
		{0.09513649344444275, -0.026980085298419, 0.000750950479414314, 0.001735975150950253, 0.002203199313953519, -0.0038941146340221167, 0.001963681075721979, -0.0020466595888137817, 0.002991508459672332, 0.0028948963154107332}; 

	for (int j = 0; j < VALID_DATA_SIZE; j++) {
		float io_tmp = 0.0;
		float qo_tmp = 0.0;
		for (int t = 0; t < NUM_TAPS; t++) {
			float tmp_i_in = iTokenIn[j+t];
			float tmp_q_in = qTokenIn[j+t];
			float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
			float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
			io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
			qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
		}
		iTokenOut[j] = io_tmp;
		qTokenOut[j] = qo_tmp;
	}
	fifoReadEnd(data->shared->inputs[FIR4_I_IN]);
	fifoReadEnd(data->shared->inputs[FIR4_Q_IN]);
	fifoWriteEnd(data->shared->outputs[FIR4_I_OUT]);
	fifoWriteEnd(data->shared->outputs[FIR4_Q_OUT]);
	return p;
}

