#include "dyndpd.h"
#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	int count;
	int length;
	shared_t *shared;
	char const *fn;
	int iteration;
	float tbuffer[NUM_TAPS-1];
} source_data_t;

int sourceInit(source_data_t *data);
void *sourceFire(void *p);
void sourceFinish(source_data_t *data);


