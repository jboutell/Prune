#include "dyndpd.h"
#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	int count;
	int length;
	shared_t *shared;
	char const *fn;
	int iteration;
	int repetitions;
} sink_data_t;

int sinkInit(sink_data_t *data);
void *sinkFire(void *p);
void sinkFinish(sink_data_t *data);

