#ifndef fir5_H
#define fir5_H

#include "common.h"

typedef struct {
	shared_t *shared;
} fir5_data_t;

int fir5Init(fir5_data_t *data);
void *fir5Fire(void *p);
void fir5Finish(fir5_data_t *data);

#endif
