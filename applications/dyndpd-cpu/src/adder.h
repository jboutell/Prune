#ifndef ADDER_H
#define ADDER_H

#include "common.h"
#include "dyndpd.h"

typedef struct {
	int _FSM_state;
	int n_ind;
	int c_ind;
	float ivect[TOKEN_SIZE];
	float qvect[TOKEN_SIZE];
	shared_t *shared;
} adder_data_t;

int adderInit(adder_data_t *data);
void *adderFire(void *p);
void adderFinish(adder_data_t *data);

#endif
