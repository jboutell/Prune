#ifndef fir7_H
#define fir7_H

#include "common.h"

typedef struct {
	shared_t *shared;
} fir7_data_t;

int fir7Init(fir7_data_t *data);
void *fir7Fire(void *p);
void fir7Finish(fir7_data_t *data);

#endif
