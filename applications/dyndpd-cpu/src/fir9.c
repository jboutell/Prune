#include "dyndpd.h"
#include "fir9.h"
#include "ports.h"

int fir9Init(fir9_data_t *data) {
	return 0;
}

void fir9Finish(fir9_data_t *data) {
}

void *fir9Fire(void *p) {

	fir9_data_t *data = (fir9_data_t *) p;
	float *iTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR9_I_IN]);
	float *qTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR9_Q_IN]);
	float *iTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR9_I_OUT]);
	float *qTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR9_Q_OUT]);

	const float fir_ic[NUM_TAPS] = 
		{0.023184288293123245, -0.003584984689950943, -0.0029193887021392584, -0.0004782175237778574, 0.0009603018988855183, 0.0013479808112606406, -0.002666295040398836, 0.0019477995811030269, -0.0010992544703185558, 0.0027281942311674356}; 
	const float fir_qc[NUM_TAPS] = 
		{0.0014246461214497685, 0.005077142734080553, -0.006542737130075693, 0.0017988578183576465, 0.0007298191194422543, 0.0012498288415372372, -0.0032778934109956026, 0.004778052680194378, -0.0051674069836735725, 0.0037808744236826897}; 

	for (int j = 0; j < VALID_DATA_SIZE; j++) {
		float io_tmp = 0.0;
		float qo_tmp = 0.0;
		for (int t = 0; t < NUM_TAPS; t++) {
			float tmp_i_in = iTokenIn[j+t];
			float tmp_q_in = qTokenIn[j+t];
			float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
			float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
			io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
			qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
		}
		iTokenOut[j] = io_tmp;
		qTokenOut[j] = qo_tmp;
	}
	fifoReadEnd(data->shared->inputs[FIR9_I_IN]);
	fifoReadEnd(data->shared->inputs[FIR9_Q_IN]);
	fifoWriteEnd(data->shared->outputs[FIR9_I_OUT]);
	fifoWriteEnd(data->shared->outputs[FIR9_Q_OUT]);
	return p;
}

