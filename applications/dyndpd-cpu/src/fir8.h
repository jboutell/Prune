#ifndef fir8_H
#define fir8_H

#include "common.h"

typedef struct {
	shared_t *shared;
} fir8_data_t;

int fir8Init(fir8_data_t *data);
void *fir8Fire(void *p);
void fir8Finish(fir8_data_t *data);

#endif
