#include "dyndpd.h"
#include "fir3.h"
#include "ports.h"

int fir3Init(fir3_data_t *data) {
	return 0;
}

void fir3Finish(fir3_data_t *data) {
}

void *fir3Fire(void *p) {

	fir3_data_t *data = (fir3_data_t *) p;
	float *iTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR3_I_IN]);
	float *qTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR3_Q_IN]);
	float *iTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR3_I_OUT]);
	float *qTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR3_Q_OUT]);

	const float fir_ic[NUM_TAPS] = 
		{0.06184425204992294, -0.02121352218091488, 0.007425661198794842, -0.0030697740148752928, 0.0022770382929593325, -0.00291186454705894, 0.0033862371928989887, -0.0034070692490786314, 0.003578966250643134, -0.003185852663591504}; 
	const float fir_qc[NUM_TAPS] = 
		{-0.07398182898759842, 0.021223928779363632, -0.001516177668236196, -0.0012676498154178262, -0.00019350471848156303, 0.0010249568149447441, -0.000111696521344129, 0.00041035976028069854, -0.00113834033254534, -0.001797760371118784}; 

	for (int j = 0; j < VALID_DATA_SIZE; j++) {
		float io_tmp = 0.0;
		float qo_tmp = 0.0;
		for (int t = 0; t < NUM_TAPS; t++) {
			float tmp_i_in = iTokenIn[j+t];
			float tmp_q_in = qTokenIn[j+t];
			float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
			float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
			io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
			qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
		}
		iTokenOut[j] = io_tmp;
		qTokenOut[j] = qo_tmp;
	}
	fifoReadEnd(data->shared->inputs[FIR3_I_IN]);
	fifoReadEnd(data->shared->inputs[FIR3_Q_IN]);
	fifoWriteEnd(data->shared->outputs[FIR3_I_OUT]);
	fifoWriteEnd(data->shared->outputs[FIR3_Q_OUT]);
	return p;
}

