#ifndef RELUL1_H
#define RELUL1_H

#include "common.h"
#include "dnn.h"

typedef struct {
	shared_t *shared;
	float *wgt;
} conv_relu_l1_data_t;

int conv_relu_l1Init(conv_relu_l1_data_t *data);
void *conv_relu_l1Fire(void *p);
void conv_relu_l1Finish(conv_relu_l1_data_t *data);

#endif
