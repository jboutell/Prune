#include "dnn.h"
#include "confL1.h"
#include <stdlib.h>
#include <string.h>
#include "ports.h"

void confL1Init(confL1_data_t *data) {
	char confL1FileName[256];
	data->file = NULL;
	sprintf(confL1FileName, "%s/%s.bin", getenv("HOME"), data->fn);
	data->iteration = 0;
	data->file = fopen(confL1FileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", confL1FileName);
		return;
	}
}

void *confL1Fire(void *p) {
	confL1_data_t *data = (confL1_data_t *) p;
	#ifdef DBGPRINT
	printf("actor confL1 fires\n");
	#endif
	char cenable;
	int enable;
	int retval = fread(&cenable, sizeof(char), 1, data->file);
	if (retval != 1) {
		printf("Source %s depleted\n", data->fn);
	}
	enable = (int) cenable;
	int *wbufout0 = (int *) fifoWriteStart(data->shared->outputs[CONFL1_OUT1_0]);
	int *wbufout1 = (int *) fifoWriteStart(data->shared->outputs[CONFL1_OUT1_1]);
	wbufout0[0] = enable;
	wbufout1[0] = enable;
	fifoWriteEnd(data->shared->outputs[CONFL1_OUT1_0]);
	fifoWriteEnd(data->shared->outputs[CONFL1_OUT1_1]);

	return p;
}

void confL1Finish(confL1_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

