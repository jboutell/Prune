#ifndef RELUL2_H
#define RELUL2_H

#include "common.h"
#include "dnn.h"

typedef struct {
	shared_t *shared;
	float *wgt;
} conv_relu_l2_data_t;

int conv_relu_l2Init(conv_relu_l2_data_t *data);
void *conv_relu_l2Fire(void *p);
void conv_relu_l2Finish(conv_relu_l2_data_t *data);

#endif
