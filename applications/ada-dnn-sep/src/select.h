#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	shared_t *shared;
	char const *fn;
	int iteration;
} select_data_t;

void selectInit(select_data_t *data);
void *selectFire(void *p);
void selectFinish(select_data_t *data);


