#include "motion.h"

__kernel void thres(__global unsigned char *rbufin1,
	__global unsigned char *rbufin2,
	__global unsigned char *wbufout1) {

	int idx;
	int tmp;
	int t1;
	int t2;
	int j;
	j = get_global_id(0);//j = 0;

	//for (j = get_local_id(0) * (PROCESSSIZE / get_local_size(0)); j < (get_local_id(0) + 1) * (PROCESSSIZE / get_local_size(0)); ) // block!
	for (j = get_global_id(0) * (PROCESSSIZE / get_global_size(0)); j < (get_global_id(0) + 1) * (PROCESSSIZE / get_global_size(0)); )
	{
		idx = j;
		t1 = rbufin1[idx];
		t2 = rbufin2[idx];
		tmp = t1 - t2;
		if (tmp < 0) {
			tmp = -tmp;
		}
		if (tmp > THRESHOLD) {
			tmp = 255;
		} else {
			tmp = 0;
		}
		wbufout1[idx] = tmp;
		j = j + 1;
	}

}


