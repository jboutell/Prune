#include "motion.h"

__kernel void med(__global unsigned char *rbufin1,
	__global unsigned char *wbufout1) {

	int idx;
	unsigned char tmp;
	unsigned char med;
	unsigned char p0;
	unsigned char p1;
	unsigned char p2;
	unsigned char p3;
	unsigned char p4;
	int j;
	j = get_global_id(0);//j = 0;

	//for (j = get_local_id(0) * (PROCESSSIZE / get_local_size(0)); j < (get_local_id(0) + 1) * (PROCESSSIZE / get_local_size(0)); ) // block!
	for (j = get_global_id(0) * (PROCESSSIZE / get_global_size(0)); j < (get_global_id(0) + 1) * (PROCESSSIZE / get_global_size(0)); )
	{
		if ((j < 1 + WIDTH || j + 1 + WIDTH > PROCESSSIZE)) {
			idx = 1 + WIDTH;
		} else {
			idx = j;
		}
		p0 = rbufin1[idx - WIDTH];
		p1 = rbufin1[idx - 1];
		p2 = rbufin1[idx];
		p3 = rbufin1[idx + 1];
		p4 = rbufin1[idx + WIDTH];
		if (p0 < p1) {
			tmp = p0;
			p0 = p1;
			p1 = tmp;
		}
		if (p2 < p3) {
			tmp = p2;
			p2 = p3;
			p3 = tmp;
		}
		if (p0 < p2) {
			tmp = p0;
			p0 = p2;
			p2 = tmp;
			tmp = p1;
			p1 = p3;
			p3 = tmp;
		}
		if (p1 < p4) {
			tmp = p1;
			p1 = p4;
			p4 = tmp;
		}
		med = p3;
		if (p1 > p2) {
			if (p2 > p4) {
				med = p2;
			} else {
				med = p4;
			}
		} else {
			if (p1 > p3) {
				med = p1;
			} else {
				med = p3;
			}
		}
		wbufout1[idx] = med;
		j = j + 1;
	}

}

