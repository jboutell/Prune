#include "motion.h"
#include "sink.h"
#include <stdlib.h>

extern volatile int globalApplicationFinished;
extern volatile int globalRepetitions;

void sinkInit(sink_data_t *data) {
	char sourceFileName[256];
	data->file = NULL;
	data->length = PROCESSSIZE;
	sprintf(sourceFileName, "%s/%s.bin", getenv("HOME"), data->fn);
	data->file = fopen(sourceFileName, "wb");
	if (globalRepetitions == 0) {
		printf("Warning: number of repetitions uninitialized\n");
	}
	data->repetitions = globalRepetitions;
	data->iteration = 0;
}

void *sinkFire(void *p) {
	sink_data_t *data = (sink_data_t *) p;
	data->iteration += REPEAT;
	if (data->file != NULL) {
		unsigned char *rbufin1 = (unsigned char *) fifoReadStart(data->shared->inputs[0]);
		fwrite(rbufin1, sizeof(cl_uchar), data->length, data->file);
		fifoReadEnd(data->shared->inputs[0]);
	}
	if(data->iteration >= data->repetitions) {
		printf("Sink finished\n");
		globalApplicationFinished = 1;
	}

	return p;
}

void sinkFinish(sink_data_t *data) {
	if (data->file != NULL) {
		fflush(data->file);
		fclose(data->file);
		data->file = NULL;
	}
}

