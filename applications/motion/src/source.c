#include "motion.h"
#include "source.h"
#include <stdlib.h>

extern volatile int globalRepetitions;

void sourceInit(source_data_t *data) {
	char sourceFileName[256];
	data->file = NULL;
	data->length = PROCESSSIZE;
	sprintf(sourceFileName, "%s/%s.bin", getenv("HOME"), data->fn);
	data->file = fopen(sourceFileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", sourceFileName);
		return;
	}
	data->iteration = 0;
	globalRepetitions = getFileSize(data->file) / FRAMESIZE;
}

void *sourceFire(void *p) {
	source_data_t *data = (source_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		printf("Source finished\n");
		actorTerminate();
	} else if (data->file != NULL) {
		unsigned char *wbufout1 = (unsigned char *) fifoWriteStart(data->shared->outputs[0]);
		int retval = fread(wbufout1, sizeof(cl_uchar), data->length, data->file);
		if (retval != data->length) {
			printf("Source %s depleted\n", data->fn);
		}
		fifoWriteEnd(data->shared->outputs[0]);
		data->iteration += REPEAT;
	}

	return p;
}

void sourceFinish(source_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

