#include "fir.h"
#include "ports.h"

#define fir6_i_out_type float*
#define fir6_q_out_type float*
#define fir6_i_in_type float*
#define fir6_q_in_type float*

__kernel void fir6(
	__global fir6_input_0_type restrict fir6_input_0_name,
	__global fir6_input_1_type restrict fir6_input_1_name,
	__global fir6_output_0_type restrict fir6_output_0_name,
	__global fir6_output_1_type restrict fir6_output_1_name) {

	const float fir_ic[NUM_TAPS] = 
		{0.03875238075852394, -0.025881294161081314, 0.019611205905675888, 0.020699821412563324, -0.058595605194568634, 0.03138734772801399, 0.01537263672798872, -0.016859745606780052, 0.00017404808022547513, 0.0021130992099642754}; 
	const float fir_qc[NUM_TAPS] = 
		{0.030343998223543167, 0.07107564061880112, -0.13356664776802063, 0.08185412734746933, 0.004377349279820919, -0.0014515514485538006, -0.0424172505736351, 0.03763338550925255, -0.009785013273358345, -0.0001437353203073144}; 

	int j = get_global_id(0);  

	float io_tmp = 0.0;
	float qo_tmp = 0.0;
	for (int t = 0; t < NUM_TAPS; t++) {
		float tmp_i_in = fir6_i_in[j+t];
		float tmp_q_in = fir6_q_in[j+t];
		float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
		float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
		io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
		qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
	}
	fir6_i_out[j] = io_tmp;
	fir6_q_out[j] = qo_tmp;
}


