#include "fir.h"
#include "ports.h"

#define fir3_i_out_type float*
#define fir3_q_out_type float*
#define fir3_i_in_type float*
#define fir3_q_in_type float*

__kernel void fir3(
	__global fir3_input_0_type restrict fir3_input_0_name,
	__global fir3_input_1_type restrict fir3_input_1_name,
	__global fir3_output_0_type restrict fir3_output_0_name,
	__global fir3_output_1_type restrict fir3_output_1_name) {

	const float fir_ic[NUM_TAPS] = 
		{0.06184425204992294, -0.02121352218091488, 0.007425661198794842, -0.0030697740148752928, 0.0022770382929593325, -0.00291186454705894, 0.0033862371928989887, -0.0034070692490786314, 0.003578966250643134, -0.003185852663591504}; 
	const float fir_qc[NUM_TAPS] = 
		{-0.07398182898759842, 0.021223928779363632, -0.001516177668236196, -0.0012676498154178262, -0.00019350471848156303, 0.0010249568149447441, -0.000111696521344129, 0.00041035976028069854, -0.00113834033254534, -0.001797760371118784}; 

	int j = get_global_id(0);  

	float io_tmp = 0.0;
	float qo_tmp = 0.0;
	for (int t = 0; t < NUM_TAPS; t++) {
		float tmp_i_in = fir3_i_in[j+t];
		float tmp_q_in = fir3_q_in[j+t];
		float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
		float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
		io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
		qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
	}
	fir3_i_out[j] = io_tmp;
	fir3_q_out[j] = qo_tmp;
}



