#include "dyndpd.h"
#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	float tbuffer[NUM_TAPS-1];
	int count;
	int length;
	int iteration;
	shared_t *shared;
	char const *fn;
} source_data_t;

int sourceInit(source_data_t *data);
void *sourceFire(void *p);
void sourceFinish(source_data_t *data);


