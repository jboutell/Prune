#include "dyndpd.h"
#include "common.h"
#include "ports.h"

void *adderIO(void *p) {
	io_data_t *data = (io_data_t *) p;

	int n_ind = data->cValue >> 8;
	int c_ind = data->cValue & 0xFF;

	data->outputEnabled[ADDER_I_OUT] = 1;
	data->outputEnabled[ADDER_Q_OUT] = 1;

	data->inputEnabled[ADDER_N_C] = 1;
	data->inputEnabled[ADDER_I_IN1] = (n_ind >= 1);
	data->inputEnabled[ADDER_Q_IN1] = (n_ind >= 1);
	data->inputEnabled[ADDER_I_IN2] = (n_ind >= 2);
	data->inputEnabled[ADDER_Q_IN2] = (n_ind >= 2);
	data->inputEnabled[ADDER_I_IN3] = (n_ind >= 3);
	data->inputEnabled[ADDER_Q_IN3] = (n_ind >= 3);
	data->inputEnabled[ADDER_I_IN4] = (n_ind >= 4);
	data->inputEnabled[ADDER_Q_IN4] = (n_ind >= 4);
	data->inputEnabled[ADDER_I_IN5] = (n_ind >= 5);
	data->inputEnabled[ADDER_Q_IN5] = (n_ind >= 5);
	data->inputEnabled[ADDER_I_IN6] = (c_ind >= 1);
	data->inputEnabled[ADDER_Q_IN6] = (c_ind >= 1);
	data->inputEnabled[ADDER_I_IN7] = (c_ind >= 2);
	data->inputEnabled[ADDER_Q_IN7] = (c_ind >= 2);
	data->inputEnabled[ADDER_I_IN8] = (c_ind >= 3);
	data->inputEnabled[ADDER_Q_IN8] = (c_ind >= 3);
	data->inputEnabled[ADDER_I_IN9] = (c_ind >= 4);
	data->inputEnabled[ADDER_Q_IN9] = (c_ind >= 4);
	data->inputEnabled[ADDER_I_INA] = (c_ind >= 5);
	data->inputEnabled[ADDER_Q_INA] = (c_ind >= 5);

	return p;
}
