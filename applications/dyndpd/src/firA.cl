#include "fir.h"
#include "ports.h"

#define firA_i_out_type float*
#define firA_q_out_type float*
#define firA_i_in_type float*
#define firA_q_in_type float*

__kernel void firA(
	__global firA_input_0_type restrict firA_input_0_name,
	__global firA_input_1_type restrict firA_input_1_name,
	__global firA_output_0_type restrict firA_output_0_name,
	__global firA_output_1_type restrict firA_output_1_name) {

	const float fir_ic[NUM_TAPS] = 
		{-0.01654597371816635, 0.004539661109447479, -0.0002926299930550158, 0.0020525960717350245, -0.002120385644957423, 0.00045400322414934635, 0.00030286467517726123, 0.0003348344471305609, -0.0009727366850711405, -0.0009426723117940128};
	const float fir_qc[NUM_TAPS] = 
		{0.0015862087020650506, -0.0033400850370526314, 0.003320542862638831, -0.0004763665492646396, -0.0007092380546964705, -0.0012016408145427704, 0.0031463124323636293, -0.004085061140358448, 0.003824063576757908, -0.0028373932000249624};

	int j = get_global_id(0);  

	float io_tmp = 0.0;
	float qo_tmp = 0.0;
	for (int t = 0; t < NUM_TAPS; t++) {
		float tmp_i_in = firA_i_in[j+t];
		float tmp_q_in = firA_q_in[j+t];
		float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
		float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
		io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
		qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
	}
	firA_i_out[j] = io_tmp;
	firA_q_out[j] = qo_tmp;
}

