#include "fir.h"
#include "ports.h"

#define adder_n_c_type int*
#define adder_i_out_type float*
#define adder_q_out_type float*
#define adder_i_in1_type float*
#define adder_q_in1_type float*
#define adder_i_in2_type float*
#define adder_q_in2_type float*
#define adder_i_in3_type float*
#define adder_q_in3_type float*
#define adder_i_in4_type float*
#define adder_q_in4_type float*
#define adder_i_in5_type float*
#define adder_q_in5_type float*
#define adder_i_in6_type float*
#define adder_q_in6_type float*
#define adder_i_in7_type float*
#define adder_q_in7_type float*
#define adder_i_in8_type float*
#define adder_q_in8_type float*
#define adder_i_in9_type float*
#define adder_q_in9_type float*
#define adder_i_inA_type float*
#define adder_q_inA_type float*

#define DPD_Adder_ic_lo -0.004622196778655052
#define DPD_Adder_qc_lo -0.004829585552215576

__kernel void adder(
	__global adder_input_0_type restrict adder_input_0_name,
	__global adder_input_1_type restrict adder_input_1_name,
	__global adder_input_2_type restrict adder_input_2_name,
	__global adder_input_3_type restrict adder_input_3_name,
	__global adder_input_4_type restrict adder_input_4_name,
	__global adder_input_5_type restrict adder_input_5_name,
	__global adder_input_6_type restrict adder_input_6_name,
	__global adder_input_7_type restrict adder_input_7_name,
	__global adder_input_8_type restrict adder_input_8_name,
	__global adder_input_9_type restrict adder_input_9_name,
	__global adder_input_10_type restrict adder_input_10_name,
	__global adder_input_11_type restrict adder_input_11_name,
	__global adder_input_12_type restrict adder_input_12_name,
	__global adder_input_13_type restrict adder_input_13_name,
	__global adder_input_14_type restrict adder_input_14_name,
	__global adder_input_15_type restrict adder_input_15_name,
	__global adder_input_16_type restrict adder_input_16_name,
	__global adder_input_17_type restrict adder_input_17_name,
	__global adder_input_18_type restrict adder_input_18_name,
	__global adder_input_19_type restrict adder_input_19_name,
	__global adder_input_20_type restrict adder_input_20_name,
	__global adder_output_0_type restrict adder_output_0_name,
	__global adder_output_1_type restrict adder_output_1_name) {

	int n_ind;
	int c_ind;
	int j;
	float ivect_tmp;
	float qvect_tmp;

	n_ind = adder_n_c[0] >> 8;
	c_ind = adder_n_c[0] & 0xFF;
	j = get_global_id(0);
	ivect_tmp = DPD_Adder_ic_lo;
	qvect_tmp = DPD_Adder_qc_lo;
	
	if (n_ind == 5) {
		ivect_tmp += adder_i_in5[j];
		qvect_tmp += adder_q_in5[j];
	}
	if (n_ind >= 4) {
		ivect_tmp += adder_i_in4[j];
		qvect_tmp += adder_q_in4[j];
	}
	if (n_ind >= 3) {
		ivect_tmp += adder_i_in3[j];
		qvect_tmp += adder_q_in3[j];
	}
	if (n_ind >= 2) {
		ivect_tmp += adder_i_in2[j];
		qvect_tmp += adder_q_in2[j];
	}
	if (n_ind >= 1) {
		ivect_tmp += adder_i_in1[j];
		qvect_tmp += adder_q_in1[j];
	}

	if (c_ind == 5) {
		ivect_tmp += adder_i_inA[j];
		qvect_tmp += adder_q_inA[j];
	}
	if (c_ind >= 4) {
		ivect_tmp += adder_i_in9[j];
		qvect_tmp += adder_q_in9[j];
	}
	if (c_ind >= 3) {
		ivect_tmp += adder_i_in8[j];
		qvect_tmp += adder_q_in8[j];
	}
	if (c_ind >= 2) {
		ivect_tmp += adder_i_in7[j];
		qvect_tmp += adder_q_in7[j];
	}
	if (c_ind >= 1) {
		ivect_tmp += adder_i_in6[j];
		qvect_tmp += adder_q_in6[j];
	}
	
	adder_i_out[j] = ivect_tmp;
	adder_q_out[j] = qvect_tmp;
}

