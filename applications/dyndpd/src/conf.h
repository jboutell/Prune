#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *iFile;
	int iLength;
	char const *fn;
	FILE *qFile;
	int qLength;
	shared_t *shared;
} conf_data_t;

int confInit(conf_data_t *data);
void *confFire(void *p);
void confFinish(conf_data_t *data);


