#include "fir.h"
#include "ports.h"

#define fir1_i_out_type float*
#define fir1_q_out_type float*
#define fir1_i_in_type float*
#define fir1_q_in_type float*

__kernel void fir1(
	__global fir1_input_0_type restrict fir1_input_0_name,
	__global fir1_input_1_type restrict fir1_input_1_name,
	__global fir1_output_0_type restrict fir1_output_0_name,
	__global fir1_output_1_type restrict fir1_output_1_name) {

	const float fir_ic[NUM_TAPS] =
		{1.0937772989273071, -0.22744165360927582, -0.07462470978498459, 0.03306879103183746, 0.05203430727124214, -0.024044763296842575, -0.03841766342520714, 0.039712920784950256, -0.012046308256685734, 0.0003442339366301894};
	const float fir_qc[NUM_TAPS] =
		{-0.051392920315265656, 0.01699735037982464, -0.00781444925814867, 0.01090577244758606, -0.011556293815374374, -0.0010344496695324779, 0.014542372897267342, -0.011368945240974426, 0.0017262097680941224, 0.0007368592196144164}; 

	int j = get_global_id(0);  

	float io_tmp = 0.0;
	float qo_tmp = 0.0;
	for (int t = 0; t < NUM_TAPS; t++) {
		float tmp_i_in = fir1_i_in[j+t];
		float tmp_q_in = fir1_q_in[j+t];
		float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
		float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
		io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
		qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
	}
	fir1_i_out[j] = io_tmp;
	fir1_q_out[j] = qo_tmp;
}


