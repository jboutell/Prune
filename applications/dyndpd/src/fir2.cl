#include "fir.h"
#include "ports.h"

#define fir2_i_out_type float*
#define fir2_q_out_type float*
#define fir2_i_in_type float*
#define fir2_q_in_type float*

__kernel void fir2(
	__global fir2_input_0_type restrict fir2_input_0_name,
	__global fir2_input_1_type restrict fir2_input_1_name,
	__global fir2_output_0_type restrict fir2_output_0_name,
	__global fir2_output_1_type restrict fir2_output_1_name) {

	const float fir_ic[NUM_TAPS] = 
		{0.1266007125377655, -0.04028431698679924, 0.013998675160109997, -0.013369462452828884, 0.01664913073182106, -0.017975620925426483, 0.015049858018755913, -0.008891353383660316, 0.0030393903143703938, -0.00015646828978788108}; 
	const float fir_qc[NUM_TAPS] = 
		{0.04336567595601082, -0.022722559049725533, 0.0252897460013628, -0.029810819774866104, 0.020235655829310417, -0.00048388622235506773, -0.013829987496137619, 0.014270305633544922, -0.0070675211027264595, 0.00205600936897099}; 

	int j = get_global_id(0);  

	float io_tmp = 0.0;
	float qo_tmp = 0.0;
	for (int t = 0; t < NUM_TAPS; t++) {
		float tmp_i_in = fir2_i_in[j+t];
		float tmp_q_in = fir2_q_in[j+t];
		float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
		float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
		io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
		qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
	}
	fir2_i_out[j] = io_tmp;
	fir2_q_out[j] = qo_tmp;
}

