#include "fir.h"
#include "ports.h"

#define fir4_i_out_type float*
#define fir4_q_out_type float*
#define fir4_i_in_type float*
#define fir4_q_in_type float*

__kernel void fir4(
	__global fir4_input_0_type restrict fir4_input_0_name,
	__global fir4_input_1_type restrict fir4_input_1_name,
	__global fir4_output_0_type restrict fir4_output_0_name,
	__global fir4_output_1_type restrict fir4_output_1_name) {

	const float fir_ic[NUM_TAPS] = 
		{-0.02557271718978882, 0.011701775714755058, -0.005951288156211376, 0.0011874770279973745, 0.00007474123412976041, 0.0016221854602918029, -0.003584664547815919, 0.005168240517377853, -0.00659256661310792, 0.006709362845867872}; 
	const float fir_qc[NUM_TAPS] = 
		{0.09513649344444275, -0.026980085298419, 0.000750950479414314, 0.001735975150950253, 0.002203199313953519, -0.0038941146340221167, 0.001963681075721979, -0.0020466595888137817, 0.002991508459672332, 0.0028948963154107332}; 

	int j = get_global_id(0);  

	float io_tmp = 0.0;
	float qo_tmp = 0.0;
	for (int t = 0; t < NUM_TAPS; t++) {
		float tmp_i_in = fir4_i_in[j+t];
		float tmp_q_in = fir4_q_in[j+t];
		float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
		float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
		io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
		qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
	}
	fir4_i_out[j] = io_tmp;
	fir4_q_out[j] = qo_tmp;
}


