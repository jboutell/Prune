#include "dyndpd.h"
#include "common.h"
#include "ports.h"

void *polyIO(void *p) {
	io_data_t *data = (io_data_t *) p;

	int n_ind = data->cValue >> 8;
	int c_ind = data->cValue & 0xFF;

	data->inputEnabled[POLY_N_C] = 1;
	data->inputEnabled[POLY_I_IN] = 1;
	data->inputEnabled[POLY_Q_IN] = 1;
	data->outputEnabled[POLY_I_OUT1] = (n_ind >= 1);
	data->outputEnabled[POLY_Q_OUT1] = (n_ind >= 1);
	data->outputEnabled[POLY_I_OUT2] = (n_ind >= 2);
	data->outputEnabled[POLY_Q_OUT2] = (n_ind >= 2);
	data->outputEnabled[POLY_I_OUT3] = (n_ind >= 3);
	data->outputEnabled[POLY_Q_OUT3] = (n_ind >= 3);
	data->outputEnabled[POLY_I_OUT4] = (n_ind >= 4);
	data->outputEnabled[POLY_Q_OUT4] = (n_ind >= 4);
	data->outputEnabled[POLY_I_OUT5] = (n_ind >= 5);
	data->outputEnabled[POLY_Q_OUT5] = (n_ind >= 5);
	data->outputEnabled[POLY_I_OUT6] = (c_ind >= 1);
	data->outputEnabled[POLY_Q_OUT6] = (c_ind >= 1);
	data->outputEnabled[POLY_I_OUT7] = (c_ind >= 2);
	data->outputEnabled[POLY_Q_OUT7] = (c_ind >= 2);
	data->outputEnabled[POLY_I_OUT8] = (c_ind >= 3);
	data->outputEnabled[POLY_Q_OUT8] = (c_ind >= 3);
	data->outputEnabled[POLY_I_OUT9] = (c_ind >= 4);
	data->outputEnabled[POLY_Q_OUT9] = (c_ind >= 4);
	data->outputEnabled[POLY_I_OUTA] = (c_ind >= 5);
	data->outputEnabled[POLY_Q_OUTA] = (c_ind >= 5);

	return p;
}
