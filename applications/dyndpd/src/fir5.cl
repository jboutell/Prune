#include "fir.h"
#include "ports.h"

#define fir5_i_out_type float*
#define fir5_q_out_type float*
#define fir5_i_in_type float*
#define fir5_q_in_type float*

__kernel void fir5(
	__global fir5_input_0_type restrict fir5_input_0_name,
	__global fir5_input_1_type restrict fir5_input_1_name,
	__global fir5_output_0_type restrict fir5_output_0_name,
	__global fir5_output_1_type restrict fir5_output_1_name) {

	const float fir_ic[NUM_TAPS] =
		{0.05645837262272835, -0.019206928089261055, 0.005519632715731859, -0.0006466842605732381, -0.0003978293971158564, -0.0009159741457551718, 0.002441369229927659, -0.003565907943993807, 0.004371261224150658, -0.00458444794639945}; 
	const float fir_qc[NUM_TAPS] = 
		{-0.11077547818422318, 0.031959958374500275, -0.0031030536629259586, -0.0010937374318018556, -0.000618482066784054, 0.0012339833192527294, -0.00001716053520794958, 0.0003248698776587844, -0.0012959156883880496, -0.001891684252768755}; 

	int j = get_global_id(0);  

	float io_tmp = 0.0;
	float qo_tmp = 0.0;
	for (int t = 0; t < NUM_TAPS; t++) {
		float tmp_i_in = fir5_i_in[j+t];
		float tmp_q_in = fir5_q_in[j+t];
		float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
		float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
		io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
		qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
	}
	fir5_i_out[j] = io_tmp;
	fir5_q_out[j] = qo_tmp;
}


